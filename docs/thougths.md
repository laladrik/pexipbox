# Bonus 2

The bonus implies to re-use data of files the application know. It requires to implement the global
index of all data chunks, index of paths to known files, index of data chunks of a file. A chunk of
4KB is indexed only. It appears the B-tree would be a good candidate to implement the data
structures but I don't involve it as:
- Go doesn't have an implementation of it in the standard library;
- it requires some benchmarks on real data;

With that let's consider hash map as its substitution as it has required interface. Its caveat in
such solution is very hard dealing with overflows.

```go
type checksum = [16]byte

type chunkref = struct {
    pathindex int 
    checkref uint64
}

type GlobalChunkIndex struct {
    lock *sync.RWMutex
    data map[checksum][]chunkref
}

type PathIndex struct {
    lock *sync.RWMutex
    data map[int]string
}

type ChunkIndex struct {
    data map[checksum][]uint64
}
```

All of the structures are stored in persistent storage and read into RAM on server startup.

## Use cases

During uploading a server reads GlobalChunkIndex while writing a file and updates GlobalChunkIndex
in the end of the uploading.

- It searches existing chunks by the strong checksum the client application sent in
  GlobalChunkIndex. The index provides references of file path and their chunk references. It gets a
  file path by the references in the PathIndex, maps the file with mmap with offset = (chunk ref) *
  4096 and length = 4096, put the data to file which is being changed.

- To update GlobalChunkIndex find all the checksums that don't get into the new version of the file.
  To do that when a file is created it has a new ChunkIndex so invalid checksums is those which are
  in the old ChunkIndex but not in the new one. As we know got invalid checksums as well as the path
  we can find them in the GlobalChunkIndex to drop them.

## Observations

All files must be accessed and changed only by the application. It should have a dedicated user in
the operation system it's running in. Only the user has write permission to the files. This allows
to implement some MVCC.

## Implementation

Implementation of the feature consists of an injection of the indexes in the uploading and removing
a file on the server side. Uploading process should be a bidirectional streaming as a client uploads
a checksum of new chunks of data, a server tries to find such chunk by the uploaded checksum. If
such chunk is found the server tells the client to send the next data otherwise it tells the client
to send the chunk of data.

### Indexing a file (ChunkIndex).

Manipulate file indexes relies on a repository with such interface:

```go
type ChunkIndexRepo interface {
    Init() error
    Get(path string, out *ChunkIndex) error
    Set(path string, index ChunkIndex) error
    Drop(path string) error
}
```

All the methods are safe for concurrent usage. An empty file is not registered in the repository.
The repository is involved in the next use cases.

#### Initialization

- [x] A folder `.meta` is created in the root folder of a server on startup then the repository keeps
  absolute path to the folder and initializes.

#### Uploading a file

- [x] A repository locks. It gets a mutex for the file or creates one, unlocks itself, acquires
  the mutex. After acquiring the mutex the repository returns ChunkIndex for the file before
  writing it.
- [x] If the file doesn't exist raise an error.
- [x] The repository saves the file ChunkIndex after writing the file.
- [x] Invalid checksums are keys which are in the returned ChunkIndex and the saved one.
- [x] Remove the invalid checksum (implies waiting of locks of the checksums from GlobalChunkIndex).
- [x] Substitute the original file by its copy built from known and uploaded
  chunks.
- [x] Recompute checksums of chunks of the entire file. It implies computing sums even for those
  chunks which were built from known chunks because if they moved at least for one byte they can't
  be found be the old reference.
- [x] The repository gains the acquired mutex.

#### Removing a file

- [x] A repository locks, creates a mutex or gets it for the file, unlock itself, acquire the
  mutex.
- [x] The repository drops the file index if it still exists. If not then skip the rest.
- [x] The repository gains the mutex.

### Indexing paths (PathIndex)

The structure helps preventing memory leaking. It can be done if entries of GlobalChunkIndex hold
pointers to the string. Different entries can hold pointers to different copies of the string.
Safety of concurrent access is done with RWMutex.

- [x] An entry in the map is added after first uploading of content of the file.
- [x] An entry is removed if the file is removed and it had some content.

### Global index of the chunks (GlobalChunkIndex)

The structure that helps to find chunks and use them to prevent a client uploading the data which
server has already.

#### Uploading a file

- [x] While a file is being uploaded a client send a message with a checksum of data it is going to
  send. The server locks its GlobalChunkIndex for reading, find an entry of the checksum, unlock
  GlobalChunkIndex, lock the entry, seek and read the data with the reference it gets from the
  entry, put the data to the building copy of the file is being uploaded, unlock the entry.
- [x] When a file is uploaded it gets an array of invalid checksums. For every such entry
  GlobalChunkIndex is locked, an entry is found by the checksum, GlobalChunkIndex is unlocked, the
  entry is locked, if the entry still exist, the reference in the array of the entry is removed.
  If it was the last reference in the entry then the entry is removed as well. The entry is
  unlocked.
- [x] Add entries from the new ChunkIndex. For every entry lock GlobalChunkIndex, add an entry or
  add an element to the array of the entry if it exists.
