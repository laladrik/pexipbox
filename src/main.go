// Package main contains two main functions: mainServer (launches server), mainClient (launches
// client). The functions validates environment variables and launches the client and the server. To
// correct the client and the server see:
// - Client entry point is ./fs/common.go
// - Server entry point is ./server/common.go
package main

import (
	"context"
	"fmt"
	"os"
	"os/signal"
	"strings"
	"syscall"
	"time"

	"gitlab.com/laladrik/pexipbox/client"
	"gitlab.com/laladrik/pexipbox/fs"
	"gitlab.com/laladrik/pexipbox/index"
	"gitlab.com/laladrik/pexipbox/input"
	plog "gitlab.com/laladrik/pexipbox/log"
	"gitlab.com/laladrik/pexipbox/server"
)

var log = plog.New("main")

func main() {
	in := input.New()
	if err := in.ValidateFolder(); err != nil {
		log.Fatal("fail validating folder", err)
	}

	sigChan := make(chan os.Signal)
	defer close(sigChan)
	signal.Notify(sigChan, syscall.SIGTERM, syscall.SIGINT)
	ctx, cancel := context.WithCancel(context.Background())
	go func() {
		fmt.Println(<-sigChan)
		cancel()
	}()

	switch in.Mode {
	case input.Server:
		mainServer(ctx, in.Folder)
	case input.Client:
		mainClient(ctx, in.Folder)
	}
}

func checkPath(value string) error {
	if value == "" {
		return fmt.Errorf("value is empty")
	}

	if _, err := os.Stat(value); err != nil {
		return fmt.Errorf("fail getting stat: %w", err)
	}

	return nil
}

func mainServer(ctx context.Context, rootpath string) {
	pathIndex := index.NewPathIndex()
	srv := server.Server{
		ChunkIndexRepo: &index.ChunkIndexRepo{
			Root: strings.Trim(os.Getenv("PEXIPBOX_META_ROOT"), " "),
		},
		PathIndex:        &pathIndex,
		GlobalChunkIndex: new(index.GlobalChunkIndex),
		Port:             2080,
		ServerKey:        strings.Trim(os.Getenv("PEXIPBOX_SSL_SERVER_KEY"), " "),
		ServerPem:        strings.Trim(os.Getenv("PEXIPBOX_SSL_SERVER_PEM"), " "),
		Cacert:           strings.Trim(os.Getenv("PEXIPBOX_SSL_CACERT_PEM"), " "),
		Root:             rootpath,
	}

	if err := checkPath(srv.ChunkIndexRepo.Root); err != nil {
		log.Fatal("PEXIPBOX_META_ROOT is invalid: ", err)
	}

	if err := checkPath(srv.ServerKey); err != nil {
		log.Fatal("PEXIPBOX_SSL_SERVER_KEY is invalid: ", err)
	}

	if err := checkPath(srv.ServerPem); err != nil {
		log.Fatal("PEXIPBOX_SSL_SERVER_PEM is invalid: ", err)
	}

	if err := checkPath(srv.Cacert); err != nil {
		log.Fatal("PEXIPBOX_SSL_CACERT_PEM is invalid: ", err)
	}

	log.Printf("Run server. PID = %d\n", os.Getpid())
	if err := srv.Init(ctx); err != nil {
		log.Fatal(err)
	}
}

func mainClient(ctx context.Context, folder string) {
	log.Printf("Run client. PID = %d\n", os.Getpid())
	cl := client.Client{
		Address:   "localhost:2080",
		ClientKey: strings.Trim(os.Getenv("PEXIPBOX_SSL_CLIENT_KEY"), " "),
		ClientPem: strings.Trim(os.Getenv("PEXIPBOX_SSL_CLIENT_PEM"), " "),
		Cacert:    strings.Trim(os.Getenv("PEXIPBOX_SSL_CACERT_PEM"), " "),
	}

	if err := checkPath(cl.ClientKey); err != nil {
		log.Fatal("PEXIPBOX_SSL_CLIENT_KEY is invalid: ", err)
	}

	if err := checkPath(cl.ClientPem); err != nil {
		log.Fatal("PEXIPBOX_SSL_CLIENT_PEM is invalid: ", err)
	}

	if err := checkPath(cl.Cacert); err != nil {
		log.Fatal("PEXIPBOX_SSL_CACERT_PEM is invalid: ", err)
	}

	if err := cl.Init(ctx, 2*time.Second); err != nil {
		log.Fatal(err)
	}

	defer func() {
		cl.Fin()
		log.Println("GRPC client is stopped")
	}()

	observe := fs.FsObserve{
		Root: folder,
	}

	pool := fs.NewWorkerPool(4, folder, &cl)
	defer pool.Fin()
	if err := observe.Run(ctx, &pool); err != nil {
		log.Fatal("fail initing the folder observer", err)
	}
}
