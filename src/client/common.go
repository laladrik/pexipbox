// Package client implements transport on top of gRPC. Client structure sends requests to create,
// delete files and upload content of a file.
package client

import (
	"context"
	"crypto/md5"
	"fmt"
	"io"
	"time"

	"gitlab.com/laladrik/pexipbox/collections"
	"gitlab.com/laladrik/pexipbox/diff"
	plog "gitlab.com/laladrik/pexipbox/log"
	pb "gitlab.com/laladrik/pexipbox/proto"
	"gitlab.com/laladrik/pexipbox/tls"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/credentials"
	"google.golang.org/grpc/encoding/gzip"
	"google.golang.org/grpc/status"
)

var log = plog.New("client")

const (
	chunkSize = 4096
	// maximum number of references to send at a time. (512 * 8 = 4096 bytes)
	refbufferLimit = 512
)

type Client struct {
	Address        string
	ClientKey      string
	ClientPem      string
	Cacert         string
	fileRepoClient pb.FileRepoClient
	conn           *grpc.ClientConn
}

// sendToStream gathers diff items from localDiff of a filedata with references
// into buffers then sends it if:
// - a diff item with a range of filedata of the file appears;
// - a buffer overflows its limit (refbufferLimit)
// - there is no more diff items.
// If a diff item with a range of filedata appears then it splits the data of
// the range into chunks of chunkSize and send it.
func (c Client) sendToStream(
	filedata []byte,
	stream pb.FileRepo_UploadClient,
	localDiff diff.Diff,
) (int, error) {
	var (
		totalSent = 0
		refBuffer = refbuffer{make([]uint64, 0), stream, refbufferLimit}
	)

	for _, diffitem := range localDiff.Items {
		if diffitem.Range == diff.EmptyRange {
			if err := refBuffer.push(diffitem.Ref); err != nil {
				return 0, fmt.Errorf("fail sending a reference to an existent chunk: %w", err)
			}

			continue
		}

		if err := refBuffer.flush(); err != nil {
			return 0, fmt.Errorf("fail flush a buffer of references")
		}

		for from := diffitem.Range[0]; from < diffitem.Range[1]; from += chunkSize {
			to := from + chunkSize
			if diffitem.Range[1] < to {
				to = diffitem.Range[1]
			}

			datalen := to - from
			toSend := filedata[from:to]
			if datalen == chunkSize {
				if err := stream.Send(pb.RegularUploadReqChecksum(md5.Sum(toSend))); err != nil {
					return 0, fmt.Errorf("fail sending a chunk of new data: %w", err)
				}

				resp, err := stream.Recv()
				if err != nil {
					return 0, fmt.Errorf("fail receiving response about existing data: %w", err)
				}

				if resp.Written > 0 {
					continue
				}
			}

			totalSent += datalen
			if err := stream.Send(pb.RegularUploadReqData(toSend)); err != nil {
				return 0, fmt.Errorf("fail sending a chunk of new data: %w", err)
			}
		}
	}

	if err := refBuffer.flush(); err != nil {
		return 0, fmt.Errorf("fail flush a buffer of references")
	}

	if err := stream.Send(pb.TermUploadReq()); err != nil {
		return 0, fmt.Errorf("fail terminating uploading: %w", err)
	}

	return totalSent, nil
}

func (c *Client) Create(ctx context.Context, path string, isDir bool, createdAt time.Time) error {
	req := pb.CreateReq{
		CreatedAt: createdAt.Unix(),
		Path:      path,
		IsDir:     isDir,
	}

	if _, err := c.fileRepoClient.Create(ctx, &req); err != nil {
		return fmt.Errorf("getting error on file creating: %w", err)
	}

	return nil
}

type UploadStat = struct {
	Sent, Uploaded int
}

// Upload sends bytes of new data and references to chunks of data that must be
// on the server. If server doesn't have a designated reference then an error
// is expected.
func (c *Client) Upload(
	ctx context.Context,
	out *UploadStat,
	path string,
	data []byte,
	localDiff diff.Diff,
) error {
	stream, err := c.fileRepoClient.Upload(ctx)
	if err != nil {
		return fmt.Errorf("fail starting uploading: %w", err)
	}

	if err := stream.Send(pb.InitUploadReq(path, localDiff.HasRefs)); err != nil {
		return fmt.Errorf("fail sending initial uploading request: %w", err)
	}

	if out.Sent, err = c.sendToStream(data, stream, localDiff); err != nil {
		return fmt.Errorf("fail reading file %+q to stream: %w", path, err)
	}

	res := &pb.UploadRes{}
	if res, err = stream.Recv(); err != nil {
		return fmt.Errorf("fail receiving result message")
	}

	out.Uploaded = int(res.Written)
	if err := stream.CloseSend(); err != nil {
		return fmt.Errorf("fail receiving result of uploading: %w", err)
	}

	return nil
}

func (c *Client) Remove(ctx context.Context, path string) error {
	if _, err := c.fileRepoClient.Remove(ctx, &pb.RemoveReq{Path: path}); err != nil {
		return fmt.Errorf("remove request is failed: %w", err)
	}
	return nil
}

type ChecksumStat = struct {
	BatchCount, ChecksumCount int
}

func (c *Client) Checksum(ctx context.Context, path string, out *collections.ChecksumIndex, stat *ChecksumStat) error {
	stream, err := c.fileRepoClient.Checksum(ctx, &pb.ChecksumReq{Path: path})
	if err != nil {
		return fmt.Errorf("fail getting stream of checksums of a remote file: %w", err)
	}

	sums := make([]*pb.ChunkID, 0)
	for {
		res, err := stream.Recv()
		switch {
		case err == io.EOF:
			*out = collections.NewChecksumIndex(sums)
			return nil
		case status.Code(err) == codes.NotFound:
			return nil
		case err != nil:
			return fmt.Errorf("fail receiving a checksum of a remote file")
		}

		sums = append(sums, res.Batch...)
		stat.BatchCount++
		stat.ChecksumCount += len(res.Batch)
	}

}

func (c *Client) FileChecksum(ctx context.Context, path string, high, low *uint64) error {
	res, err := c.fileRepoClient.FileChecksum(ctx, &pb.FileChecksumReq{Path: path})
	if err != nil {
		return fmt.Errorf("fail getting a checksum of a file, %w", err)
	}

	*high = res.High
	*low = res.Low
	return nil
}

func (c *Client) Fin() {
	if c.conn != nil {
		c.conn.Close()
	}
}

func (c *Client) Init(ctx context.Context, timeout time.Duration) error {
	var (
		err                error
		cred               = new(credentials.TransportCredentials)
		timeoutctx, cancel = context.WithTimeout(ctx, timeout)
	)

	defer cancel()
	grpc.WithDefaultCallOptions(grpc.UseCompressor(gzip.Name))
	if err := tls.Credentials(cred, c.ClientPem, c.ClientKey, c.Cacert); err != nil {
		return fmt.Errorf("fail getting TLS credentials: %w", err)
	}

	c.conn, err = grpc.DialContext(
		timeoutctx,
		c.Address,
		grpc.WithTransportCredentials(*cred),
		grpc.WithBlock(),
	)

	if err != nil {
		return fmt.Errorf("fail dialing the server: %w", err)
	}

	c.fileRepoClient = pb.NewFileRepoClient(c.conn)
	return nil
}
