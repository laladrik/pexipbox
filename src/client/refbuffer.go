package client

import (
	"fmt"

	pb "gitlab.com/laladrik/pexipbox/proto"
)

type refbuffer struct {
	buf    []uint64
	stream pb.FileRepo_UploadClient
	limit  int
}

func (r *refbuffer) push(ref uint64) error {
	r.buf = append(r.buf, ref)
	if len(r.buf) == r.limit {
		return r.flush()
	}

	return nil
}

func (r *refbuffer) flush() error {
	if len(r.buf) == 0 {
		return nil
	}

	if err := r.stream.Send(pb.RegularUploadReqRef(r.buf)); err != nil {
		return fmt.Errorf("fail sending a reference to an existent chunk: %w", err)
	}

	r.buf = make([]uint64, 0)
	return nil
}

func newrefbuffer(stream pb.FileRepo_UploadClient, limit int) refbuffer {
	return refbuffer{make([]uint64, 0), stream, limit}
}
