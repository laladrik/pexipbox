package main

import (
	"io"
	"io/ioutil"
	"os"
	"syscall"
	"testing"
)

func BenchmarkSeqRead(b *testing.B) {
	out := ioutil.Discard
	fd, err := os.Open("zeros")
	if err != nil {
		log.Fatal(err)
	}

	defer fd.Close()
	total := 0
	buf := make([]byte, 4096)
	for {
		read, err := fd.Read(buf)
		if err != nil {
			if err == io.EOF {
				break
			}
			log.Fatal(err)
		}

		written, err := out.Write(buf[:read])
		if err != nil {
			log.Fatal(err)
		}

		total += written
	}
}

func BenchmarkMmap(b *testing.B) {
	out := ioutil.Discard
	fd, err := os.Open("zeros")
	if err != nil {
		b.Fatal(err)
	}

	defer fd.Close()
	stat, err := fd.Stat()
	if err != nil {
		b.Fatal(err)
	}

	data, err := syscall.Mmap(int(fd.Fd()), 0, int(stat.Size()), syscall.PROT_NONE, syscall.MAP_PRIVATE)
	if err != nil {
		b.Fatal(err)
	}

	defer syscall.Munmap(data)
	total := 0
	for i := 0; i < len(data); i += 4096 {
		written, err := out.Write(data[i : i+4096])
		if err != nil {
			b.Fatal(err)
		}

		total += written
	}
}

func BenchmarkMiniMmap(b *testing.B) {
	out := ioutil.Discard
	fd, err := os.Open("zeros")
	if err != nil {
		b.Fatal(err)
	}

	defer fd.Close()
	stat, err := fd.Stat()
	if err != nil {
		b.Fatal(err)
	}

	total := 0
	filesize := int(stat.Size())
	for i := 0; i < filesize; i += 4096 {
		mapSize := 4096
		if i+mapSize > filesize {
			mapSize = filesize - i
		}

		data, err := syscall.Mmap(int(fd.Fd()), int64(0), mapSize, syscall.PROT_NONE, syscall.MAP_PRIVATE)
		if err != nil {
			b.Fatal(err)
		}

		syscall.Munmap(data)
		written, err := out.Write(data)
		if err != nil {
			b.Fatal(err)
		}

		total += written
	}
}
