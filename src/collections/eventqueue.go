package collections

import (
	"fmt"
	"sync"

	"github.com/fsnotify/fsnotify"
)

var NoEvent = fmt.Errorf("no events")

// EventQueue is FIFO which notifies about new elements.
type EventQueue struct {
	newEvent chan struct{}
	lock     sync.Mutex
	events   []fsnotify.Event
}

func (q *EventQueue) Push(ev fsnotify.Event) {
	q.lock.Lock()
	defer q.lock.Unlock()
	q.events = append(q.events, ev)
	select {
	case q.newEvent <- struct{}{}:
	default:
	}
}

func (q *EventQueue) Pop(out *fsnotify.Event) error {
	q.lock.Lock()
	defer q.lock.Unlock()
	if len(q.events) > 0 {
		*out = q.events[0]
		q.events = q.events[1:]
		return nil
	}

	return NoEvent
}

func (q *EventQueue) Wait() <-chan struct{} {
	return q.newEvent
}

func (q *EventQueue) Fin() {
	close(q.newEvent)
}

func NewEventQueue() *EventQueue {
	return &EventQueue{
		newEvent: make(chan struct{}, 1),
		events:   make([]fsnotify.Event, 0, 10),
	}
}
