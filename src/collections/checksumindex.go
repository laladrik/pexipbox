package collections

import (
	"sort"

	pb "gitlab.com/laladrik/pexipbox/proto"
)

// ChecksumIndex stores index of pairs weak and strong checksums sorted by weak
// one. The index stores start positions of sequences of the ckecksums with the
// same weak one. A sequence can accesses with method Get which returns an
// iterator of the sequence.
type ChecksumIndex struct {
	index map[uint32]int
	data  []pb.ChunkID
}

type ChecksumIter struct {
	weak   uint32
	data   []pb.ChunkID
	cursor int
}

func (c *ChecksumIter) Next(out *pb.ChunkID) bool {
	if c.cursor >= len(c.data) || c.weak != c.data[c.cursor].Weak {
		return false
	}

	*out = c.data[c.cursor]
	c.cursor++
	return true
}

func (c ChecksumIndex) Get(weak uint32) ChecksumIter {
	return ChecksumIter{
		data:   c.data[c.index[weak]:],
		weak:   weak,
		cursor: 0,
	}
}

func NewChecksumIndex(input []*pb.ChunkID) ChecksumIndex {
	ret := ChecksumIndex{}
	if len(input) == 0 {
		return ret
	}

	ret.data = make([]pb.ChunkID, len(input))
	ret.index = make(map[uint32]int)
	for i := range input {
		ret.data[i] = *input[i]
	}

	sort.Slice(ret.data, func(i, j int) bool {
		return ret.data[i].Weak < ret.data[j].Weak
	})

	prev := ret.data[0].Weak
	ret.index[prev] = 0
	for i, sum := range ret.data {
		if sum.Weak == prev {
			continue
		}

		prev = sum.Weak
		ret.index[prev] = i
	}

	return ret
}
