package log

import (
	"fmt"
	"log"
	"os"
)

type Logger struct {
	inner *log.Logger
}

func (l *Logger) Println(args ...interface{}) {
	l.inner.Println(args...)
}

func (l *Logger) Printf(fmt string, args ...interface{}) {
	l.inner.Printf(fmt, args...)
}

func (l *Logger) Fatal(args ...interface{}) {
	l.inner.Fatal(args...)
}

func New(prefix string) Logger {
	stat, err := os.Stdout.Stat()
	if err != nil {
		panic(err)
	}

	if stat.Mode()&os.ModeCharDevice == os.ModeCharDevice {
		prefix = fmt.Sprintf("\x1b[34m%s\x1b[0m", prefix)
	}

	return Logger{
		inner: log.New(os.Stdout, prefix+" ", log.LstdFlags|log.Lmsgprefix),
	}
}
