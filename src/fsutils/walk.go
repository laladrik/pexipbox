package fsutils

import (
	"context"
	"fmt"
	"os"
	"path"
)

func Walk(ctx context.Context, dir string, callback func(string) error) error {
	if isDone(ctx) {
		return nil
	}

	fd, err := os.Open(dir)
	if err != nil {
		return fmt.Errorf("fail openning %+q: %w", dir, err)
	}

	files, err := fd.Readdir(0)
	if err != nil {
		return fmt.Errorf("fail reading file list of %+q: %w", dir, err)
	}

	for _, file := range files {
		filepath := path.Join(dir, file.Name())
		if err := callback(filepath); err != nil {
			return err
		}

		if file.IsDir() {
			if err := Walk(ctx, filepath, callback); err != nil {
				return err
			}
		}
	}

	return nil
}

func isDone(ctx context.Context) bool {
	select {
	case <-ctx.Done():
		return true
	default:
		return false
	}
}
