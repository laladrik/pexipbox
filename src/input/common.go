package input

import (
	"flag"
	"fmt"
	"log"
	"os"
)

type Input struct {
	Mode, Folder string
}

type Mode = string

const (
	Server Mode = `server`
	Client      = `client`
)

func (i Input) ValidateFolder() error {
	stat, err := os.Stat(i.Folder)
	switch {
	case os.IsNotExist(err):
		if err := os.Mkdir(i.Folder, 0775); err != nil {
			return fmt.Errorf("fail creating a folder `%s`: %w", i.Folder, err)
		}

	case !stat.IsDir():
		return fmt.Errorf("designated path `%s` is not a folder", i.Folder)
	}

	return nil
}

func New() Input {
	ret := Input{}
	flag.StringVar(&ret.Mode, `m`, ``, `application mode. Possible values: server, client`)
	flag.StringVar(&ret.Folder, `f`, ``, `path to observable folder`)
	flag.Parse()
	if ret.Mode != Server && ret.Mode != Client {
		log.Fatalf("unexpected application mode value: %+q", ret.Mode)
	}

	if ret.Folder == `` {
		log.Fatalf("observable folder can't be empty")
	}

	return ret
}
