package diff

var EmptyRange = [2]int{-1, -1}

type DiffItem = struct {
	// Reference to a chunk of a file. Chunk is 4096 bytes.
	Ref uint64
	// Range [from:to) of changed bytes.
	Range [2]int
}

type Diff struct {
	HasRefs bool
	Items   []DiffItem
}

func (m *Diff) pushRef(v uint64) {
	m.HasRefs = true
	m.Items = append(m.Items, DiffItem{Ref: v, Range: EmptyRange})
}

func (m *Diff) pushRange(v int) {
	if len(m.Items) == 0 || m.Items[len(m.Items)-1].Range == EmptyRange {
		m.Items = append(m.Items, DiffItem{Range: [2]int{v, v + 1}})
	}

	m.Items[len(m.Items)-1].Range[1] = v + 1
}
