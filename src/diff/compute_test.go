package diff

import (
	"bytes"
	"crypto/md5"
	"io"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/laladrik/pexipbox/collections"
	"gitlab.com/laladrik/pexipbox/hash"
	pb "gitlab.com/laladrik/pexipbox/proto"
)

func TestVerify(t *testing.T) {
	var (
		checksums = func(from io.Reader) []*pb.ChunkID {
			ret := make([]*pb.ChunkID, 0)
			for i := uint64(0); ; i++ {
				buf := make([]byte, 4096)
				n, err := from.Read(buf)
				if err == io.EOF {
					return ret
				}

				assert.NoError(t, err)
				strongHash := md5.Sum(buf[:n])
				high, low := hash.MD5littleEndian(strongHash)
				ret = append(ret, &pb.ChunkID{
					Ref:        i,
					Weak:       hash.NewRolling(buf[:n]).Sum(),
					StrongHigh: high,
					StrongLow:  low,
				})
			}
		}
	)

	t.Run("entire middle chunk changed", func(t *testing.T) {
		var (
			assert          = assert.New(t)
			diff            = Diff{Items: make([]DiffItem, 0)}
			firstSame       = bytes.Repeat([]byte{10}, 4096)
			secondRemote    = bytes.Repeat([]byte{11}, 4096)
			secondLocal     = bytes.Repeat([]byte{12}, 4096)
			thirdSame       = bytes.Repeat([]byte{30}, 4096)
			remoteChecksums = collections.NewChecksumIndex(
				checksums(bytes.NewBuffer(bytes.Join([][]byte{firstSame, secondRemote, thirdSame}, nil))),
			)
			localData   = bytes.Join([][]byte{firstSame, secondLocal, thirdSame}, nil)
			localBuffer = bytes.NewBuffer(localData)
		)

		Compute(&diff, localBuffer, remoteChecksums)
		assert.Equal(DiffItem{Ref: 0, Range: EmptyRange}, diff.Items[0])
		assert.Equal(DiffItem{Ref: 0, Range: [2]int{4096, 8192}}, diff.Items[1])
		assert.Equal(DiffItem{Ref: 2, Range: EmptyRange}, diff.Items[2])
		assert.True(diff.HasRefs)
	})

	t.Run("file push front", func(t *testing.T) {
		var (
			assert          = assert.New(t)
			diff            = Diff{Items: make([]DiffItem, 0)}
			common          = bytes.Repeat([]byte{10}, 4096)
			remoteChecksums = collections.NewChecksumIndex(checksums(bytes.NewBuffer(common)))
			localBuffer     = bytes.NewBuffer(bytes.Join([][]byte{{0, 1, 2}, common}, nil))
		)

		Compute(&diff, localBuffer, remoteChecksums)
		assert.Equal(DiffItem{Ref: 0, Range: [2]int{0, 3}}, diff.Items[0])
		assert.Equal(DiffItem{Ref: 0, Range: EmptyRange}, diff.Items[1])
		assert.True(diff.HasRefs)
	})

	t.Run("file push back", func(t *testing.T) {
		var (
			assert          = assert.New(t)
			diff            = Diff{Items: make([]DiffItem, 0)}
			common          = bytes.Repeat([]byte{10}, 4096)
			remoteData      = bytes.NewBuffer(common)
			remoteChecksums = collections.NewChecksumIndex(checksums(remoteData))
			localData       = bytes.Join([][]byte{common, {0, 1, 2}}, nil)
			localBuffer     = bytes.NewBuffer(localData)
		)

		Compute(&diff, localBuffer, remoteChecksums)
		assert.Equal(DiffItem{Ref: 0, Range: EmptyRange}, diff.Items[0])
		assert.Equal(DiffItem{Ref: 0, Range: [2]int{4096, 4099}}, diff.Items[1])
		assert.True(diff.HasRefs)
	})

	t.Run("file insert", func(t *testing.T) {
		var (
			assert          = assert.New(t)
			diff            = Diff{Items: make([]DiffItem, 0)}
			commonHead      = bytes.Repeat([]byte{10}, 4096)
			commonTail      = bytes.Repeat([]byte{20}, 4096)
			remoteData      = bytes.NewBuffer(bytes.Join([][]byte{commonHead, commonTail}, nil))
			remoteChecksums = collections.NewChecksumIndex(checksums(remoteData))
			localBuffer     = bytes.NewBuffer(bytes.Join([][]byte{commonHead, {0, 1, 2}, commonTail}, nil))
		)

		Compute(&diff, localBuffer, remoteChecksums)
		assert.Equal(DiffItem{Ref: 0, Range: EmptyRange}, diff.Items[0])
		assert.Equal(DiffItem{Ref: 0, Range: [2]int{4096, 4099}}, diff.Items[1])
		assert.Equal(DiffItem{Ref: 1, Range: EmptyRange}, diff.Items[2])
		assert.True(diff.HasRefs)
	})

	t.Run("small file", func(t *testing.T) {
		var (
			assert          = assert.New(t)
			diff            = Diff{Items: make([]DiffItem, 0)}
			common          = []byte{1, 2, 3}
			remoteData      = bytes.NewBuffer(common)
			remoteChecksums = collections.NewChecksumIndex(checksums(remoteData))
			localBuffer     = bytes.NewBuffer(append(common, 3, 2, 1))
		)

		Compute(&diff, localBuffer, remoteChecksums)
		assert.False(diff.HasRefs)
		assert.Equal(DiffItem{Ref: 0, Range: [2]int{0, 6}}, diff.Items[0])
	})

	t.Run("big new file", func(t *testing.T) {
		var (
			assert          = assert.New(t)
			diff            = Diff{Items: make([]DiffItem, 0)}
			remoteData      = bytes.NewBuffer([]byte{})
			remoteChecksums = collections.NewChecksumIndex(checksums(remoteData))
			localBuffer     = bytes.NewBuffer(bytes.Repeat([]byte{10}, 4097))
		)

		Compute(&diff, localBuffer, remoteChecksums)
		assert.False(diff.HasRefs)
		assert.Equal(DiffItem{Ref: 0, Range: [2]int{0, 4097}}, diff.Items[0])
	})
}
