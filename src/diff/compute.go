package diff

import (
	"crypto/md5"
	"fmt"
	"io"

	"gitlab.com/laladrik/pexipbox/collections"
	"gitlab.com/laladrik/pexipbox/hash"
	pb "gitlab.com/laladrik/pexipbox/proto"
)

type source interface {
	Read([]byte) (int, error)
	ReadByte() (byte, error)
}

// Compute find changes in local finding weak checksum matches in checksums.
// Results are collected in out.
//
// - It fetches an entire queueBuf or just one byte every iteration.
// - If checksum of queueBuf is found in checksums then a reference of the
// chunk of the checksum is saved to out, so the entire queueBuf is going to be
// fetched on the next iteration. Otherwise position of the first byte of the
// queueBuf is saved to out, so only on byte is going to be fetched on the next
// iteration.
// - If a checksum of the last chunk of local is not found in checksums then
// queueBuf is saved to out.
func Compute(out *Diff, local source, checksums collections.ChecksumIndex) error {
	var (
		queueBuf               = make([]byte, 4096)
		diffStart, queueBufLen = 0, 0
		isLastMatched          = true
		rollingSum             = new(hash.Rolling)
		err                    error
		nextByte               byte
		chunkRef               uint64
	)

	for {
		if isLastMatched {
			queueBufLen, err = local.Read(queueBuf)
		} else {
			nextByte, err = local.ReadByte()
			queueBuf = queueBuf[1:]
			queueBuf = append(queueBuf, nextByte)
		}

		switch {
		case err == io.EOF:
			// flush unmatched data
			if !isLastMatched {
				out.pushRange(diffStart + queueBufLen - 2)
			}
			return nil
		case err != nil:
			return fmt.Errorf("fail reading source: %w", err)
		}

		read := queueBuf[:queueBufLen] // queueBufLen is not changed if isLastMatched == true
		if isLastMatched {
			*rollingSum = hash.NewRolling(read)
		} else {
			rollingSum.Push(nextByte)
		}

		if chunkRef, isLastMatched = find(read, rollingSum.Sum(), checksums); isLastMatched {
			out.pushRef(chunkRef)
			diffStart += queueBufLen
		} else {
			out.pushRange(diffStart)
			diffStart++
		}
	}
}

// Find matchings of data in knownsums:
// - find all matchings of weak sums.
// - verifies the matchings by a strong checksum. Computing strong checksum is expensive
// that's why it's done after first match.
//
// This is implementation of [1] But knownsums has hash table from standard
// library intead of 16bit hash table of weak checksums under the hood. This is
// potential pitfall as the implementation computes hash of datasum to lookup a
// weak sum in the hash table of knownsums. Dropbox uses hash table from Rust
// standard library in the implementation of their rsync library [2].
//
// [1] https://rsync.samba.org/tech_report/node4.html
// [2] https://github.com/dropbox/fast_rsync/blob/master/src/signature.rs#L36
func find(data []byte, datasum uint32, knownsums collections.ChecksumIndex) (uint64, bool) {
	results := knownsums.Get(datasum)
	out := new(pb.ChunkID)
	if !results.Next(out) {
		return 0, false
	}

	strongHash := md5.Sum(data)
	high, low := hash.MD5littleEndian(strongHash)
	if low == out.StrongLow && high == out.StrongHigh {
		return out.Ref, true
	}

	for results.Next(out) {
		if low == out.StrongLow && high == out.StrongHigh {
			return out.Ref, true
		}
	}

	return 0, false
}
