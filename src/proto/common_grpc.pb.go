// Code generated by protoc-gen-go-grpc. DO NOT EDIT.

package pexipbox

import (
	context "context"

	grpc "google.golang.org/grpc"
	codes "google.golang.org/grpc/codes"
	status "google.golang.org/grpc/status"
)

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
// Requires gRPC-Go v1.32.0 or later.
const _ = grpc.SupportPackageIsVersion7

// FileRepoClient is the client API for FileRepo service.
//
// For semantics around ctx use and closing/ending streaming RPCs, please refer to https://pkg.go.dev/google.golang.org/grpc/?tab=doc#ClientConn.NewStream.
type FileRepoClient interface {
	Create(ctx context.Context, in *CreateReq, opts ...grpc.CallOption) (*CreateRes, error)
	Upload(ctx context.Context, opts ...grpc.CallOption) (FileRepo_UploadClient, error)
	Remove(ctx context.Context, in *RemoveReq, opts ...grpc.CallOption) (*RemoveRes, error)
	Checksum(ctx context.Context, in *ChecksumReq, opts ...grpc.CallOption) (FileRepo_ChecksumClient, error)
	FileChecksum(ctx context.Context, in *FileChecksumReq, opts ...grpc.CallOption) (*FileChecksumRes, error)
}

type fileRepoClient struct {
	cc grpc.ClientConnInterface
}

func NewFileRepoClient(cc grpc.ClientConnInterface) FileRepoClient {
	return &fileRepoClient{cc}
}

func (c *fileRepoClient) Create(ctx context.Context, in *CreateReq, opts ...grpc.CallOption) (*CreateRes, error) {
	out := new(CreateRes)
	err := c.cc.Invoke(ctx, "/pexipbox.FileRepo/Create", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *fileRepoClient) Upload(ctx context.Context, opts ...grpc.CallOption) (FileRepo_UploadClient, error) {
	stream, err := c.cc.NewStream(ctx, &FileRepo_ServiceDesc.Streams[0], "/pexipbox.FileRepo/Upload", opts...)
	if err != nil {
		return nil, err
	}
	x := &fileRepoUploadClient{stream}
	return x, nil
}

type FileRepo_UploadClient interface {
	Send(*UploadReq) error
	Recv() (*UploadRes, error)
	grpc.ClientStream
}

type fileRepoUploadClient struct {
	grpc.ClientStream
}

func (x *fileRepoUploadClient) Send(m *UploadReq) error {
	return x.ClientStream.SendMsg(m)
}

func (x *fileRepoUploadClient) Recv() (*UploadRes, error) {
	m := new(UploadRes)
	if err := x.ClientStream.RecvMsg(m); err != nil {
		return nil, err
	}
	return m, nil
}

func (c *fileRepoClient) Remove(ctx context.Context, in *RemoveReq, opts ...grpc.CallOption) (*RemoveRes, error) {
	out := new(RemoveRes)
	err := c.cc.Invoke(ctx, "/pexipbox.FileRepo/Remove", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *fileRepoClient) Checksum(ctx context.Context, in *ChecksumReq, opts ...grpc.CallOption) (FileRepo_ChecksumClient, error) {
	stream, err := c.cc.NewStream(ctx, &FileRepo_ServiceDesc.Streams[1], "/pexipbox.FileRepo/Checksum", opts...)
	if err != nil {
		return nil, err
	}
	x := &fileRepoChecksumClient{stream}
	if err := x.ClientStream.SendMsg(in); err != nil {
		return nil, err
	}
	if err := x.ClientStream.CloseSend(); err != nil {
		return nil, err
	}
	return x, nil
}

type FileRepo_ChecksumClient interface {
	Recv() (*ChecksumRes, error)
	grpc.ClientStream
}

type fileRepoChecksumClient struct {
	grpc.ClientStream
}

func (x *fileRepoChecksumClient) Recv() (*ChecksumRes, error) {
	m := new(ChecksumRes)
	if err := x.ClientStream.RecvMsg(m); err != nil {
		return nil, err
	}
	return m, nil
}

func (c *fileRepoClient) FileChecksum(ctx context.Context, in *FileChecksumReq, opts ...grpc.CallOption) (*FileChecksumRes, error) {
	out := new(FileChecksumRes)
	err := c.cc.Invoke(ctx, "/pexipbox.FileRepo/FileChecksum", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

// FileRepoServer is the server API for FileRepo service.
// All implementations must embed UnimplementedFileRepoServer
// for forward compatibility
type FileRepoServer interface {
	Create(context.Context, *CreateReq) (*CreateRes, error)
	Upload(FileRepo_UploadServer) error
	Remove(context.Context, *RemoveReq) (*RemoveRes, error)
	Checksum(*ChecksumReq, FileRepo_ChecksumServer) error
	FileChecksum(context.Context, *FileChecksumReq) (*FileChecksumRes, error)
	mustEmbedUnimplementedFileRepoServer()
}

// UnimplementedFileRepoServer must be embedded to have forward compatible implementations.
type UnimplementedFileRepoServer struct {
}

func (UnimplementedFileRepoServer) Create(context.Context, *CreateReq) (*CreateRes, error) {
	return nil, status.Errorf(codes.Unimplemented, "method Create not implemented")
}
func (UnimplementedFileRepoServer) Upload(FileRepo_UploadServer) error {
	return status.Errorf(codes.Unimplemented, "method Upload not implemented")
}
func (UnimplementedFileRepoServer) Remove(context.Context, *RemoveReq) (*RemoveRes, error) {
	return nil, status.Errorf(codes.Unimplemented, "method Remove not implemented")
}
func (UnimplementedFileRepoServer) Checksum(*ChecksumReq, FileRepo_ChecksumServer) error {
	return status.Errorf(codes.Unimplemented, "method Checksum not implemented")
}
func (UnimplementedFileRepoServer) FileChecksum(context.Context, *FileChecksumReq) (*FileChecksumRes, error) {
	return nil, status.Errorf(codes.Unimplemented, "method FileChecksum not implemented")
}
func (UnimplementedFileRepoServer) mustEmbedUnimplementedFileRepoServer() {}

// UnsafeFileRepoServer may be embedded to opt out of forward compatibility for this service.
// Use of this interface is not recommended, as added methods to FileRepoServer will
// result in compilation errors.
type UnsafeFileRepoServer interface {
	mustEmbedUnimplementedFileRepoServer()
}

func RegisterFileRepoServer(s grpc.ServiceRegistrar, srv FileRepoServer) {
	s.RegisterService(&FileRepo_ServiceDesc, srv)
}

func _FileRepo_Create_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(CreateReq)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(FileRepoServer).Create(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/pexipbox.FileRepo/Create",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(FileRepoServer).Create(ctx, req.(*CreateReq))
	}
	return interceptor(ctx, in, info, handler)
}

func _FileRepo_Upload_Handler(srv interface{}, stream grpc.ServerStream) error {
	return srv.(FileRepoServer).Upload(&fileRepoUploadServer{stream})
}

type FileRepo_UploadServer interface {
	Send(*UploadRes) error
	Recv() (*UploadReq, error)
	grpc.ServerStream
}

type fileRepoUploadServer struct {
	grpc.ServerStream
}

func (x *fileRepoUploadServer) Send(m *UploadRes) error {
	return x.ServerStream.SendMsg(m)
}

func (x *fileRepoUploadServer) Recv() (*UploadReq, error) {
	m := new(UploadReq)
	if err := x.ServerStream.RecvMsg(m); err != nil {
		return nil, err
	}
	return m, nil
}

func _FileRepo_Remove_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(RemoveReq)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(FileRepoServer).Remove(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/pexipbox.FileRepo/Remove",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(FileRepoServer).Remove(ctx, req.(*RemoveReq))
	}
	return interceptor(ctx, in, info, handler)
}

func _FileRepo_Checksum_Handler(srv interface{}, stream grpc.ServerStream) error {
	m := new(ChecksumReq)
	if err := stream.RecvMsg(m); err != nil {
		return err
	}
	return srv.(FileRepoServer).Checksum(m, &fileRepoChecksumServer{stream})
}

type FileRepo_ChecksumServer interface {
	Send(*ChecksumRes) error
	grpc.ServerStream
}

type fileRepoChecksumServer struct {
	grpc.ServerStream
}

func (x *fileRepoChecksumServer) Send(m *ChecksumRes) error {
	return x.ServerStream.SendMsg(m)
}

func _FileRepo_FileChecksum_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(FileChecksumReq)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(FileRepoServer).FileChecksum(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/pexipbox.FileRepo/FileChecksum",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(FileRepoServer).FileChecksum(ctx, req.(*FileChecksumReq))
	}
	return interceptor(ctx, in, info, handler)
}

// FileRepo_ServiceDesc is the grpc.ServiceDesc for FileRepo service.
// It's only intended for direct use with grpc.RegisterService,
// and not to be introspected or modified (even as a copy)
var FileRepo_ServiceDesc = grpc.ServiceDesc{
	ServiceName: "pexipbox.FileRepo",
	HandlerType: (*FileRepoServer)(nil),
	Methods: []grpc.MethodDesc{
		{
			MethodName: "Create",
			Handler:    _FileRepo_Create_Handler,
		},
		{
			MethodName: "Remove",
			Handler:    _FileRepo_Remove_Handler,
		},
		{
			MethodName: "FileChecksum",
			Handler:    _FileRepo_FileChecksum_Handler,
		},
	},
	Streams: []grpc.StreamDesc{
		{
			StreamName:    "Upload",
			Handler:       _FileRepo_Upload_Handler,
			ServerStreams: true,
			ClientStreams: true,
		},
		{
			StreamName:    "Checksum",
			Handler:       _FileRepo_Checksum_Handler,
			ServerStreams: true,
		},
	},
	Metadata: "common.proto",
}
