package pexipbox

import (
	"bytes"
	"crypto/md5"
	"encoding/binary"
)

func InitUploadReq(path string, hasrefs bool) *UploadReq {
	return &UploadReq{
		Request: &UploadReq_Init{
			Init: &UploadReqInit{
				Path: path, HasRefs: hasrefs,
			},
		},
	}
}

func RegularUploadReqData(data []byte) *UploadReq {
	return &UploadReq{
		Request: &UploadReq_Regular{
			Regular: &UploadReqRegular{
				Kind: UploadReqRegularKind_DATA,
				Data: data,
			},
		},
	}
}

func RegularUploadReqRef(refs []uint64) *UploadReq {
	databuf := new(bytes.Buffer)
	binary.Write(databuf, binary.LittleEndian, refs)
	return &UploadReq{
		Request: &UploadReq_Regular{
			Regular: &UploadReqRegular{
				Kind: UploadReqRegularKind_REFS,
				Data: databuf.Bytes(),
			},
		},
	}
}

func RegularUploadReqChecksum(data [md5.Size]byte) *UploadReq {
	return &UploadReq{
		Request: &UploadReq_Regular{
			Regular: &UploadReqRegular{
				Kind: UploadReqRegularKind_CHECKSUM,
				Data: data[:],
			},
		},
	}
}

func TermUploadReq() *UploadReq {
	return &UploadReq{
		Request: &UploadReq_Term{
			Term: new(UploadReqTerm),
		},
	}
}
