package server

import (
	"bytes"
	"crypto/md5"
	"io/ioutil"
	"os"
	"path"
	"testing"

	_ "github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/require"
	"gitlab.com/laladrik/pexipbox/hash"
	"gitlab.com/laladrik/pexipbox/index"
	pb "gitlab.com/laladrik/pexipbox/proto"
	pbmock "gitlab.com/laladrik/pexipbox/proto/mocks"
	"gitlab.com/laladrik/pexipbox/server"
)

func TestUpload(t *testing.T) {
	type setupRes = struct {
		metaRoot, dataRoot string
		server             server.Server
	}

	var (
		setup = func(t *testing.T) setupRes {
			metaRoot, err := ioutil.TempDir("", "")
			require.Nil(t, err)
			dataRoot, err := ioutil.TempDir("", "")
			require.Nil(t, err)
			pathIndex := index.NewPathIndex()
			chunkIndexRepo := index.ChunkIndexRepo{Root: metaRoot}
			globalChunkIndex := new(index.GlobalChunkIndex)
			globalChunkIndex.Init()
			require.Nil(t, chunkIndexRepo.Init())
			return setupRes{
				metaRoot: metaRoot,
				dataRoot: dataRoot,
				server: server.Server{
					ChunkIndexRepo:   &chunkIndexRepo,
					PathIndex:        &pathIndex,
					GlobalChunkIndex: globalChunkIndex,
					Root:             dataRoot,
				},
			}
		}

		teardown = func(data setupRes) {
			os.RemoveAll(data.metaRoot)
			os.RemoveAll(data.dataRoot)
		}

		chunkof = func(this byte) []byte {
			return bytes.Repeat([]byte{this}, 4096)
		}

		makeChecksums = func(chunks [][]byte) []index.Checksum {
			ret := make([][16]byte, 0)
			for _, chunk := range chunks {
				ret = append(ret, md5.Sum(chunk))
			}
			return ret
		}

		doesFileContains = func(t *testing.T, filepath string, data ...[]byte) error {
			targetFileData, err := ioutil.ReadFile(filepath)
			require.NoError(t, err)
			from, to := 0, 0
			for _, chunk := range data {
				to += len(chunk)
				require.Equal(t, chunk, targetFileData[from:to])
				from = to
			}
			return nil
		}
	)

	t.Run("full_upload", func(t *testing.T) {
		const (
			requestDataCount   = 3
			finishMessageCount = 1
		)

		var (
			require        = require.New(t)
			setupData      = setup(t)
			uploadstream   = new(pbmock.FileRepo_UploadServer)
			srv            = setupData.server
			relativePath   = "0001.dat"
			parts          = [3][]byte{chunkof(10), chunkof(20), chunkof(30)}
			checksums      = makeChecksums(parts[:])
			clientRequests = []*pb.UploadReq{
				pb.InitUploadReq(relativePath, false),
				pb.RegularUploadReqChecksum(checksums[0]),
				pb.RegularUploadReqData(parts[0]),
				pb.RegularUploadReqChecksum(checksums[1]),
				pb.RegularUploadReqData(parts[1]),
				pb.RegularUploadReqChecksum(checksums[2]),
				pb.RegularUploadReqData(parts[2]),
				pb.TermUploadReq(),
			}
			recvCallCount = 0
			chunkIndex    = index.NewChunkIndex()
		)

		defer teardown(setupData)
		uploadstream.On("Send", &pb.UploadRes{Written: 0}).Return(nil)
		uploadstream.On("Send", &pb.UploadRes{Written: 4096 * 3}).Return(nil)
		uploadstream.On("Recv").Return(func() *pb.UploadReq {
			ret := clientRequests[recvCallCount]
			recvCallCount++
			return ret
		}, nil)

		require.Nil(srv.Upload(uploadstream))
		uploadstream.AssertNumberOfCalls(t, "Send", requestDataCount+finishMessageCount)
		uploadstream.AssertNumberOfCalls(t, "Recv", len(clientRequests))
		require.Equal(recvCallCount, len(clientRequests))
		doesFileContains(t, path.Join(srv.Root, relativePath), parts[:]...)
		_, err := os.Stat(path.Join(srv.ChunkIndexRepo.Root, relativePath))
		require.NoError(err)
		require.NoError(srv.ChunkIndexRepo.Get(relativePath, &chunkIndex))
		require.Equal(map[index.Checksum]index.ChunkIndexEntry{
			checksums[0]: {hash.RollingSum(parts[0]), []uint64{0}},
			checksums[1]: {hash.RollingSum(parts[1]), []uint64{1}},
			checksums[2]: {hash.RollingSum(parts[2]), []uint64{2}},
		}, chunkIndex.Data)
		pathIndex, ok := srv.PathIndex.Get(relativePath)
		require.True(ok)
		require.Equal(0, pathIndex)
		ref := new(index.GlobalChunkRef)
		require.True(srv.GlobalChunkIndex.Get(checksums[0], ref))
		require.Equal(index.GlobalChunkRef{0, 0}, *ref)
		require.True(srv.GlobalChunkIndex.Get(checksums[1], ref))
		require.Equal(index.GlobalChunkRef{0, 1}, *ref)
		require.True(srv.GlobalChunkIndex.Get(checksums[2], ref))
		require.Equal(index.GlobalChunkRef{0, 2}, *ref)
	})

	t.Run("partial_upload", func(t *testing.T) {
		t.Run("with_global_index", func(t *testing.T) {
			const (
				requestDataCount   = 1
				approveDataCount   = 1
				finishMessageCount = 1
			)

			var (
				require             = require.New(t)
				setupData           = setup(t)
				fullUploadStream    = new(pbmock.FileRepo_UploadServer)
				partialUploadStream = new(pbmock.FileRepo_UploadServer)
				srv                 = setupData.server
				parts               = [3][]byte{chunkof(10), chunkof(20), chunkof(30)}
				checksums           = makeChecksums(parts[:])
				fullUploadFile      = "0001.dat"
				partialUploadFile   = "0002.dat"
				newPart             = chunkof(12)
				newChecksum         = md5.Sum(newPart)
				fullUploadRequests  = []*pb.UploadReq{
					pb.InitUploadReq(fullUploadFile, false),
					pb.RegularUploadReqChecksum(checksums[0]),
					pb.RegularUploadReqData(parts[0]),
					pb.RegularUploadReqChecksum(checksums[1]),
					pb.RegularUploadReqData(parts[1]),
					pb.RegularUploadReqChecksum(checksums[2]),
					pb.RegularUploadReqData(parts[2]),
					pb.TermUploadReq(),
				}
				partialUploadRequests = []*pb.UploadReq{
					pb.InitUploadReq(partialUploadFile, false),
					pb.RegularUploadReqChecksum(checksums[0]),
					pb.RegularUploadReqData(newPart),
					pb.RegularUploadReqChecksum(newChecksum),
					pb.TermUploadReq(),
				}
				fullRequestCount    = 0
				partialRequestCount = 0
				chunkIndex          = index.NewChunkIndex()
			)

			defer teardown(setupData)
			fullUploadStream.On("Send", &pb.UploadRes{Written: 0}).Return(nil)
			fullUploadStream.On("Send", &pb.UploadRes{Written: 4096}).Return(nil)
			fullUploadStream.On("Send", &pb.UploadRes{Written: 4096 * 3}).Return(nil)
			fullUploadStream.On("Recv").Return(func() *pb.UploadReq {
				ret := fullUploadRequests[fullRequestCount]
				fullRequestCount++
				return ret
			}, nil)

			partialUploadStream.On("Send", &pb.UploadRes{Written: 0}).Return(nil)
			partialUploadStream.On("Send", &pb.UploadRes{Written: 4096}).Return(nil)
			partialUploadStream.On("Send", &pb.UploadRes{Written: 4096 * 2}).Return(nil)
			partialUploadStream.On("Recv").Return(func() *pb.UploadReq {
				ret := partialUploadRequests[partialRequestCount]
				partialRequestCount++
				return ret
			}, nil)

			require.NoError(srv.Upload(fullUploadStream))
			require.NoError(srv.Upload(partialUploadStream))
			partialUploadStream.AssertNumberOfCalls(t, "Send", requestDataCount+finishMessageCount+approveDataCount)

			doesFileContains(t, path.Join(srv.Root, partialUploadFile), parts[0], newPart)
			_, err := os.Stat(path.Join(srv.ChunkIndexRepo.Root, partialUploadFile))
			require.NoError(err)
			require.NoError(srv.ChunkIndexRepo.Get(partialUploadFile, &chunkIndex))
			require.Equal(map[index.Checksum]index.ChunkIndexEntry{
				checksums[0]: {hash.RollingSum(parts[0]), []uint64{0}},
				newChecksum:  {hash.RollingSum(newPart), []uint64{1}},
			}, chunkIndex.Data)

			pathIndex, ok := srv.PathIndex.Get(partialUploadFile)
			require.True(ok)
			require.Equal(1, pathIndex)
			ref := new(index.GlobalChunkRef)
			require.True(srv.GlobalChunkIndex.Get(checksums[0], ref))
			require.Equal(index.GlobalChunkRef{0, 0}, *ref)
			require.True(srv.GlobalChunkIndex.Get(newChecksum, ref))
			require.Equal(index.GlobalChunkRef{1, 1}, *ref)
		})

		t.Run("no_global_index", func(t *testing.T) {
		})
	})
}
