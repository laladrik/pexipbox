package upload

import (
	"fmt"
	"io"
	"io/ioutil"
	"os"
	"syscall"

	"gitlab.com/laladrik/pexipbox/index"
)

type transactionState = int

const (
	inProgress transactionState = iota
	committed
	rollbacked

	partSuffix = ".part"
)

var (
	errRollbacked = fmt.Errorf("transaction already rollbacked")
	errCommitted  = fmt.Errorf("transaction already committed")
)

// If a transaction rollbacks then target is not changed, backup has the initial data and the
// data which it not committed but marked as the new data.
type transaction struct {
	// targetSize    int64
	backup *os.File
	// file consists of uploaded data. It is renamed if a transaction commits.
	part *os.File
	// path to file which is changed if a transaction commits.
	target           string
	state            transactionState
	newDataRanges    [][2]int
	chunkIndex       ChunkIndex
	targetReadUnlock func()
}

// - Drops those entries from the t.chunkIndex.GlobalChunkIndex which are going to be invalid.
// - Gets checksum and references which is going to be added to the global chunk index.
// - Sets exclusive lock on t.target to don't screw up other reader/writers of the file.
// - Renames .part file to t.target.
// - Unsets the exclusive lock.
// - Add entries to the global chunk index leads to the new parts of the file.
//
// NOTE: Such 2-step updating of the global chunk index is required. If it is updated before
// the renaming if has invalid references to the .part file. It if is updated after the renaming it
// has invalid references to old chunks which does not exist in the .part file.
//
// TODO: rollback the global index update if an error occurs between the first and the last step.
func (t *transaction) commit() error {
	switch t.state {
	case committed:
		return nil
	case rollbacked:
		return errRollbacked
	}

	t.state = committed
	t.fin()
	additions := make(map[index.Checksum][]uint64)
	newIndex := index.NewChunkIndex()
	if err := newIndex.Init(t.target + partSuffix); err != nil {
		return fmt.Errorf("fail building index: %w", err)
	}

	if err := t.chunkIndex.partialUpdate(newIndex, additions); err != nil {
		return fmt.Errorf("fail updating chunk index partially: %w", err)
	}

	pathIndex, ok := t.chunkIndex.PathIndex.Get(t.chunkIndex.RelativePath)
	if !ok {
		return fmt.Errorf("fail getting index of uploaded file: %+q", t.target)
	}

	t.targetReadUnlock()
	_, unlock := t.chunkIndex.PathIndex.Lock(pathIndex)
	if err := os.Rename(t.target+partSuffix, t.target); err != nil {
		return fmt.Errorf("fail repalicing target by part: %w", err)
	}

	unlock()
	if err := t.chunkIndex.apply(additions); err != nil {
		return fmt.Errorf("fail adding new chunks to the global chunk index: %w", err)
	}

	if err := os.Remove(t.backup.Name()); err != nil {
		log.Printf("fail removing backup file: %+v\n", err)
	}

	return nil
}

func (t *transaction) backupNew() (*os.File, error) {
	var (
		isOk       = false
		err        error
		mapSize    int64
		targetData []byte
		tmp        *os.File
	)

	if _, err := t.part.Seek(0, io.SeekStart); err != nil {
		return nil, fmt.Errorf("fail seek part to its start: %w", err)
	}

	if mapSize, err = filesize(t.part); err != nil {
		return nil, fmt.Errorf("fail getting size of part: %w", err)
	}

	fd, size := int(t.part.Fd()), int(mapSize)
	if targetData, err = syscall.Mmap(fd, 0, size, syscall.PROT_READ, syscall.MAP_PRIVATE); err != nil {
		return nil, fmt.Errorf("fail mmap part: %w", err)
	}

	if tmp, err = ioutil.TempFile("", ""); err != nil {
		return nil, fmt.Errorf("fail creating a temp file: %w", err)
	}

	defer func() {
		if !isOk {
			tmp.Close()
			os.Remove(tmp.Name())
		}
	}()

	for _, item := range t.newDataRanges {
		if _, err := tmp.Write(targetData[item[0]:item[1]]); err != nil {
			return nil, fmt.Errorf("fail writing to temp file: %w", err)
		}
	}

	if _, err := tmp.Seek(0, io.SeekStart); err != nil {
		return nil, fmt.Errorf("fail seek temp file to start: %w", err)
	}

	isOk = true
	return tmp, nil
}

// rollback tries to save data marked as new from .part file to .backup file.
func (t *transaction) rollback() error {
	switch t.state {
	case rollbacked:
		return nil
	case committed:
		return errCommitted
	}

	t.state = rollbacked
	defer t.fin()
	uncommited, err := t.backupNew()
	if err != nil {
		log.Printf("uncommited data is not persisted: %+v", err)
	} else {
		defer func() {
			uncommited.Close()
			if err := os.Remove(uncommited.Name()); err != nil {
				log.Printf("fail removing uncommited data: %+v\n", err)
			}
		}()
	}

	t.targetReadUnlock()
	if uncommited != nil {
		if _, err := io.Copy(t.backup, uncommited); err != nil {
			log.Println("fail appending backup with uncommited data.")
		}
	}

	return nil
}

func (t *transaction) fin() {
	t.backup.Close()
	t.part.Close()
}

func (t *transaction) init(backupSuffix string) error {
	var (
		backupSize int64
		err        error
		isOk       = false
		partPath   = t.target + partSuffix
		backupPath = t.target + backupSuffix
	)

	defer func() {
		if !isOk {
			switch {
			case t.part != nil:
				t.part.Close()
				fallthrough
			case t.backup != nil:
				t.backup.Close()
			}
		}
	}()

	index, ok := t.chunkIndex.PathIndex.Get(t.chunkIndex.RelativePath)
	if !ok {
		return fmt.Errorf("fail getting index of %+q", t.chunkIndex.RelativePath)
	}

	_, t.targetReadUnlock = t.chunkIndex.PathIndex.RLock(index)
	if t.targetReadUnlock == nil {
		return fmt.Errorf("lock for %+q (index = %d) does not exist", t.chunkIndex.RelativePath, index)
	}

	if t.part, err = os.OpenFile(partPath, os.O_RDWR|os.O_CREATE|os.O_TRUNC, 0644); err != nil {
		return fmt.Errorf("fail openning part file: %w", err)
	}

	if t.backup, err = os.OpenFile(backupPath, os.O_RDWR|os.O_APPEND|os.O_CREATE, 0644); err != nil {
		return fmt.Errorf("fail creating temp file: %w", err)
	}

	if backupSize, err = filesize(t.backup); err != nil {
		return fmt.Errorf("fail getting")
	}

	if backupSize == 0 {
		target, err := os.Open(t.target)
		if err != nil {
			return fmt.Errorf("fail openning origin file: %w", err)
		}
		defer target.Close()

		if _, err = io.Copy(t.backup, target); err != nil {
			return fmt.Errorf("fail copying origin to temp file: %w", err)
		}
	}

	isOk = true
	return nil
}

func (t *transaction) markNew(from, to int) {
	if t.newDataRanges == nil {
		t.newDataRanges = make([][2]int, 0, 1)
	}

	t.newDataRanges = append(t.newDataRanges, [2]int{from, to})
}

func (t *transaction) Write(data []byte) (int, error) {
	return t.part.Write(data)
}
