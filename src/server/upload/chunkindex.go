package upload

import (
	"fmt"
	"os"
	"path"

	"gitlab.com/laladrik/pexipbox/index"
)

type ChunkIndex struct {
	GlobalChunkIndex *index.GlobalChunkIndex
	PathIndex        *index.PathIndex
	ChunkIndexRepo   *index.ChunkIndexRepo
	RelativePath     string
	Root             string
}

func (i *ChunkIndex) create() error {
	absolutePath := path.Join(i.Root, i.RelativePath)
	fd, err := os.Open(absolutePath)
	if err != nil {
		return fmt.Errorf("fail openning a file to index it: %w", err)
	}

	defer fd.Close()
	newIndex := index.NewChunkIndex()
	if err := newIndex.Compute(fd); err != nil {
		return fmt.Errorf("fail building index: %w", err)
	}

	if err := i.ChunkIndexRepo.Set(i.RelativePath, newIndex); err != nil {
		return fmt.Errorf("fail saving index: %w", err)
	}

	pathIndex := i.PathIndex.Set(i.RelativePath)
	for sum, entry := range newIndex.Data {
		for _, ref := range entry.Refs {
			i.GlobalChunkIndex.Add(sum, index.GlobalChunkRef{pathIndex, ref})
		}
	}

	return nil
}

func (i *ChunkIndex) apply(additions map[index.Checksum][]uint64) error {
	pathIndex, ok := i.PathIndex.Get(i.RelativePath)
	if !ok {
		return fmt.Errorf("path was not indexed before")
	}

	for sum, reflist := range additions {
		for _, ref := range reflist {
			i.GlobalChunkIndex.Add(sum, index.GlobalChunkRef{pathIndex, ref})
		}
	}

	return nil
}

func (i *ChunkIndex) partialUpdate(newIndex index.ChunkIndex, additions map[index.Checksum][]uint64) error {
	oldIndex := index.NewChunkIndex()
	if err := i.ChunkIndexRepo.Get(i.RelativePath, &oldIndex); err != nil {
		return fmt.Errorf("fail retrieving index: %w", err)
	}

	if err := i.ChunkIndexRepo.Set(i.RelativePath, newIndex); err != nil {
		return fmt.Errorf("fail retrieving index: %w", err)
	}

	pathIndex, ok := i.PathIndex.Get(i.RelativePath)
	if !ok {
		return fmt.Errorf("path was not indexed before")
	}

	onAddition := func(sum index.Checksum, ref uint64) error {
		i.GlobalChunkIndex.Add(sum, index.GlobalChunkRef{pathIndex, ref})
		return nil
	}

	onRemoval := func(sum index.Checksum, ref uint64) error {
		if _, ok := additions[sum]; !ok {
			additions[sum] = make([]uint64, 0, 1)
		}

		additions[sum] = append(additions[sum], ref)
		return nil
	}

	oldIndex.Diff(newIndex, onAddition, onRemoval)
	return nil
}
