package upload

import (
	"fmt"
	"io"
	"os"
	"path"

	"gitlab.com/laladrik/pexipbox/index"
)

var errNotFound = fmt.Errorf("chunk not found")

type ChunkRepo struct {
	GlobalChunkIndex *index.GlobalChunkIndex
	PathIndex        *index.PathIndex
	Root             string
}

// Get gets chunk of data by checksum.
func (repo *ChunkRepo) Get(checksum index.Checksum, out []byte) (int, error) {
	chunkref := new(index.GlobalChunkRef)
	if repo.GlobalChunkIndex.Get(checksum, chunkref) {
		filepath, unlock := repo.PathIndex.RLock(chunkref.Pathindex)
		if unlock == nil {
			return 0, fmt.Errorf("pathIndex points to file that does not exist")
		}

		defer unlock()
		file, err := os.Open(path.Join(repo.Root, filepath))
		if err != nil {
			return 0, err
		}

		defer file.Close()
		if _, err := file.Seek(int64(chunkref.Chunkref)*4096, io.SeekStart); err != nil {
			return 0, err
		}

		return file.Read(out)
	}

	return 0, errNotFound
}
