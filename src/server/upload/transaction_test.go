package upload

import (
	"bytes"
	"io/ioutil"
	"os"
	"path/filepath"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/laladrik/pexipbox/index"
)

func TestTransaction(t *testing.T) {
	type setupRes = struct {
		pathIndex        *index.PathIndex
		chunkIndexRepo   *index.ChunkIndexRepo
		globalChunkIndex *index.GlobalChunkIndex
		metaRoot         string
	}
	var (
		temp = func(data ...[]byte) string {
			file, err := ioutil.TempFile("", "")
			require.Nil(t, err)
			defer file.Close()
			for _, item := range data {
				_, err = file.Write(item)
				require.Nil(t, err)
			}
			return file.Name()
		}

		setup = func(t *testing.T) setupRes {
			metaRoot, err := ioutil.TempDir("", "")
			require.Nil(t, err)

			pathIndex := index.NewPathIndex()
			chunkIndexRepo := index.ChunkIndexRepo{Root: metaRoot}
			globalChunkIndex := new(index.GlobalChunkIndex)
			globalChunkIndex.Init()
			require.NoError(t, chunkIndexRepo.Init())

			return setupRes{
				pathIndex:        &pathIndex,
				chunkIndexRepo:   &chunkIndexRepo,
				globalChunkIndex: globalChunkIndex,
				metaRoot:         metaRoot,
			}
		}

		teardown = func(data setupRes) {
			os.RemoveAll(data.metaRoot)
		}

		errOnly = func(_ interface{}, err error) error {
			return err
		}
	)

	t.Run("rollback", func(t *testing.T) {
		var (
			assert     = assert.New(t)
			require    = require.New(t)
			setupData  = setup(t)
			commonHead = bytes.Repeat([]byte{10}, 6)
			commonTail = bytes.Repeat([]byte{20}, 6)
			newPiece   = bytes.Repeat([]byte{11}, 6)
			origin     = temp(commonHead, commonTail)
			tx         = transaction{
				target: origin,
				chunkIndex: ChunkIndex{
					setupData.globalChunkIndex,
					setupData.pathIndex,
					setupData.chunkIndexRepo,
					filepath.Base(origin),
					filepath.Dir(origin),
				},
			}
		)

		defer teardown(setupData)
		require.NoError(tx.chunkIndex.create())
		require.Nil(tx.init(".backup"))
		require.Nil(errOnly(tx.Write(commonHead)))
		require.Nil(errOnly(tx.Write(newPiece)))
		tx.markNew(6, 12)
		require.Nil(tx.rollback())
		targetData, err := ioutil.ReadFile(tx.target)
		assert.Nil(err)
		assert.Equal(targetData, bytes.Join([][]byte{commonHead, commonTail}, nil))
		backupData, err := ioutil.ReadFile(tx.backup.Name())
		assert.Nil(err)
		assert.Equal(bytes.Join([][]byte{commonHead, commonTail, newPiece}, nil), backupData)
	})

	t.Run("commit", func(t *testing.T) {
		var (
			assert     = assert.New(t)
			require    = require.New(t)
			commonHead = bytes.Repeat([]byte{10}, 6)
			commonTail = bytes.Repeat([]byte{20}, 6)
			newPiece   = bytes.Repeat([]byte{11}, 6)
			origin     = temp(commonHead, commonTail)
			setupData  = setup(t)
			tx         = transaction{
				target: origin,
				chunkIndex: ChunkIndex{
					setupData.globalChunkIndex,
					setupData.pathIndex,
					setupData.chunkIndexRepo,
					filepath.Base(origin),
					filepath.Dir(origin),
				},
			}
		)

		defer teardown(setupData)
		require.NoError(tx.chunkIndex.create())
		require.Nil(tx.init(".backup"))
		require.Nil(errOnly(tx.Write(commonHead)))
		require.Nil(errOnly(tx.Write(newPiece)))
		tx.markNew(6, 12)
		require.Nil(errOnly(tx.Write(commonTail)))
		require.Nil(tx.commit())
		targetData, err := ioutil.ReadFile(tx.target)
		assert.Nil(err)
		assert.Equal(targetData, bytes.Join([][]byte{commonHead, newPiece, commonTail}, nil))
		assert.True(os.IsNotExist(errOnly(os.Stat(tx.backup.Name()))))
	})
}
