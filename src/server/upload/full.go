package upload

import (
	"fmt"
	"io"
	"os"

	"gitlab.com/laladrik/pexipbox/index"
	pb "gitlab.com/laladrik/pexipbox/proto"

	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

func Full(dst string, stream pb.FileRepo_UploadServer, repo ChunkRepo, chunkIndex ChunkIndex) (int, error) {
	var (
		total   = 0
		written = 0
	)

	outfile, err := os.OpenFile(dst, os.O_CREATE|os.O_WRONLY|os.O_TRUNC, 0644)
	if err != nil {
		return 0, raiseInternalError("fail creating a file while uploading", err)
	}

	defer outfile.Close()
loop:
	for {
		req, err := stream.Recv()
		switch {
		case err == io.EOF:
			// Consider unexpected EOF as success to commit uploaded data. If a client tries to
			// upload the file again it will get checksums of what it uploaded this time.
			break loop
		case err != nil:
			return 0, raiseInternalError("fail reading upload request", err)
		case req.GetTerm() != nil:
			break loop
		case req.GetRegular() == nil:
			return 0, status.Error(codes.InvalidArgument, "UploadReqRegular message must be sent")
		case req.GetRegular().GetData() == nil:
			return 0, status.Error(codes.InvalidArgument, "data is empty")
		}

		switch req.GetRegular().Kind {
		case pb.UploadReqRegularKind_CHECKSUM:
			checksum := index.Checksum{}
			if size := copy(checksum[:], req.GetRegular().GetData()); size != len(checksum) {
				return 0, status.Errorf(
					codes.InvalidArgument,
					"checksum length is not equal to %d",
					len(checksum),
				)
			}

			buf := make([]byte, 4096)
			n, err := repo.Get(checksum, buf)
			if err != nil {
				if err != errNotFound {
					log.Printf("fail getting data of checksum %x: %v", checksum, err)
				}

				if err := stream.Send(&pb.UploadRes{Written: 0}); err != nil {
					return 0, raiseInternalError("fail requesting data: %w", err)
				}
				continue
			}

			if written, err = outfile.Write(buf[:n]); err != nil {
				return 0, raiseInternalError(fmt.Sprintf("fail writing file, request %+v", req), err)
			}

			if err := stream.Send(&pb.UploadRes{Written: uint64(n)}); err != nil {
				return 0, raiseInternalError("fail approving existing of data: %w", err)
			}

		case pb.UploadReqRegularKind_DATA:
			if written, err = outfile.Write(req.GetRegular().GetData()); err != nil {
				return 0, raiseInternalError(fmt.Sprintf("fail writing file, request %+v", req), err)
			}
		}

		total += written

	}

	if err := chunkIndex.create(); err != nil {
		log.Println("fail creating chunkIndex", err)
	}

	return total, nil
}
