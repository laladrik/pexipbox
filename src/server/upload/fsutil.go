package upload

import (
	"io"
	"os"
)

func copyHead(dst, src *os.File, size int64) error {
	if err := truncate(dst); err != nil {
		return err
	}

	if _, err := src.Seek(0, io.SeekStart); err != nil {
		return err
	}

	if _, err := io.CopyN(dst, src, size); err != nil {
		return err
	}

	return nil
}

func truncate(fd *os.File) error {
	if _, err := fd.Seek(0, io.SeekStart); err != nil {
		return err
	}

	if err := fd.Truncate(0); err != nil {
		return err
	}

	return nil
}

func filesize(fd *os.File) (int64, error) {
	stat, err := fd.Stat()
	return stat.Size(), err
}
