package upload

import (
	"bytes"
	"encoding/binary"
	"io"
	"syscall"

	plog "gitlab.com/laladrik/pexipbox/log"
	pb "gitlab.com/laladrik/pexipbox/proto"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

var (
	log    = plog.New("server.upload")
	iosize = 4096
)

func raiseInternalError(msg string, err error) error {
	log.Println(msg, "-", err)
	return status.Error(codes.Internal, "Internal error")
}

// Partial make dst backup if it doesn't exist. Reads data from stream.
//
// - If an incoming message contains a batch of references to chunks of dst then it reads the chunks
// from the backup then writes the chunks to .part file.
// - If a message contains a chunk of bytes then it writes it to the .part file.
// - If a message contains a checksum then it tries to get a chunk from GlobalChunkIndex and write
//
// it to the .part file. If a chunk is not found then it requests the chunk the checksum belongs.
// If the uploaded file is committed then its backup is removed and .part file replaces origin.
// Otherwise backup is kept to prevent re-uploading of uploaded data and .part file is removed.
func Partial(
	dst,
	backupSuffix string,
	stream pb.FileRepo_UploadServer,
	repo ChunkRepo,
	chunkIndex ChunkIndex,
) (int, error) {
	tx := &transaction{
		chunkIndex: chunkIndex,
		target:     dst,
	}

	if err := tx.init(backupSuffix); err != nil {
		return 0, raiseInternalError("fail initializing an uploading transaction: %w", err)
	}

	defer func() {
		if err := tx.rollback(); err != nil && err != errCommitted {
			log.Println("fail rollbacking an uploading transaction", err)
		}
	}()

	refFile := tx.backup
	stat, err := refFile.Stat()
	if err != nil {
		return 0, raiseInternalError("fail getting stat of a reference file", err)
	}

	size := int(stat.Size()) // bug for 32-bit OS.
	refMap, err := syscall.Mmap(int(refFile.Fd()), 0, size, syscall.PROT_READ, syscall.MAP_PRIVATE)
	if err != nil {
		return 0, raiseInternalError("fail creating mmap to a reference file", err)
	}

	defer syscall.Munmap(refMap)
	total := 0
loop:
	for {
		req, err := stream.Recv()
		switch {
		case err == io.EOF:
			break loop
		case err != nil:
			return 0, raiseInternalError("fail reading upload request", err)
		case req.GetTerm() != nil:
			break loop
		case req.GetRegular() == nil:
			return 0, status.Error(codes.InvalidArgument, "UploadReqRegular message must be sent")
		}

		var data []byte
		switch req.GetRegular().Kind {
		case pb.UploadReqRegularKind_DATA:
			data = req.GetRegular().Data
			n, err := tx.Write(data)
			if err != nil {
				return 0, raiseInternalError("fail writing uploaded to a file", err)
			}

			tx.markNew(total, total+n)
			total += n
		case pb.UploadReqRegularKind_REFS:
			refs := make([]uint64, len(req.GetRegular().Data)/8)
			binary.Read(bytes.NewReader(req.GetRegular().Data), binary.LittleEndian, refs)
			for _, ref := range refs {
				from := iosize * int(ref)
				to := from + iosize
				if len(refMap) < to {
					to = len(refMap)
				}

				n, err := tx.Write(refMap[from:to])
				if err != nil {
					return 0, raiseInternalError("fail writing uploaded to a file", err)
				}

				total += n
			}

		default:
			return 0, status.Errorf(
				codes.Unimplemented,
				"unexpected type of payload of upload regular request: %s",
				req.GetRegular().Kind.String(),
			)
		}
	}

	if err := tx.commit(); err != nil {
		return 0, raiseInternalError("fail committing uploading transaction", err)
	}

	return total, nil
}
