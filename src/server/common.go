// Package implements handling incoming messages. It deletes and creates messages, receiving
// uploaded content of files is implemented in ./upload/full.go ./upload/partial.go. Structure
// used to optimize amount of uploaded data are in index/globalchunkindex.go
package server

import (
	"context"
	"fmt"
	"io"
	"net"
	"os"
	"path"
	"syscall"

	"gitlab.com/laladrik/pexipbox/hash"
	"gitlab.com/laladrik/pexipbox/index"
	plog "gitlab.com/laladrik/pexipbox/log"
	pb "gitlab.com/laladrik/pexipbox/proto"
	"gitlab.com/laladrik/pexipbox/server/upload"
	"gitlab.com/laladrik/pexipbox/tls"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/credentials"
	_ "google.golang.org/grpc/encoding/gzip"
	"google.golang.org/grpc/status"
)

var (
	log    = plog.New("server")
	iosize = 4096
	// maximum number of checksums to send at a time. (16 * (64 + 64 + 64 + 32) = 3584 bytes)
	checksumLimit = 16
)

const backupSuffix = ".backup"

type Server struct {
	pb.UnimplementedFileRepoServer
	PathIndex        *index.PathIndex
	ChunkIndexRepo   *index.ChunkIndexRepo
	GlobalChunkIndex *index.GlobalChunkIndex
	Port             uint16
	ServerKey        string
	ServerPem        string
	Cacert           string
	Root             string
}

func raiseInternalError(msg string, err error) error {
	log.Println(msg, "-", err)
	return status.Error(codes.Internal, "Internal error")
}

func (s Server) Create(ctx context.Context, req *pb.CreateReq) (*pb.CreateRes, error) {
	if _, err := os.Stat(path.Join(s.Root, req.Path)); err == nil {
		return &pb.CreateRes{}, nil
	}

	if req.IsDir {
		if err := os.Mkdir(path.Join(s.Root, req.Path), 0775); err != nil {
			return nil, raiseInternalError("fail creating a folder", err)
		}
	} else {
		fd, err := os.Create(path.Join(s.Root, req.Path))
		if err != nil {
			return nil, raiseInternalError("fail creating a file", err)
		}
		fd.Close()
	}

	return &pb.CreateRes{}, nil
}

func (s Server) createFile(relativePath string) (*os.File, error) {
	absolutePath := path.Join(s.Root, relativePath)
	fd, err := os.OpenFile(absolutePath, os.O_CREATE|os.O_WRONLY|os.O_TRUNC, 0644)
	if err != nil {
		return nil, fmt.Errorf("fail openning openning file: %w", err)
	}

	return fd, nil
}

// Upload receives messages from stream. It waits for initial uploading request
// (UploadReq.UploadReqInit), which has a relative path of a file and flag indicates if the stream
// has references to chunks of existing copy of the file on the server. If there is no references
// then a full (./upload/full.go) uploading process runs otherwise partial (./upload/partial.go)
// uploading process. Any of the processes waits for:
// - a regular uploading request (UploadReq.UploadReqRegular) with a checksum of
// the chunk is going to be uploaded. If such chunk is found in any of file on the server then it
// sends a response (UploadRes) with number of written bytes > 0 otherwise it sends a message with 0.
// - a regular uploading request with a chunk of bytes. It saves them.
// Partial uploading process receives references to existing chunks of the file. If such messages is
// received then the data is saved from chunk.
func (s *Server) Upload(stream pb.FileRepo_UploadServer) error {
	var (
		total int
		err   error
	)

	init, err := stream.Recv()
	switch {
	case err == io.EOF:
		return nil
	case err != nil:
		return raiseInternalError("fail getting initial upload request", err)
	case init.GetInit() == nil:
		return status.Error(codes.InvalidArgument, "invalid initial request to upload")
	}

	initreq := init.GetInit()
	absolutePath := path.Join(s.Root, initreq.Path)
	chunkRepo := upload.ChunkRepo{
		GlobalChunkIndex: s.GlobalChunkIndex,
		PathIndex:        s.PathIndex,
		Root:             s.Root,
	}
	chunkIndex := upload.ChunkIndex{
		GlobalChunkIndex: s.GlobalChunkIndex,
		PathIndex:        s.PathIndex,
		ChunkIndexRepo:   s.ChunkIndexRepo,
		RelativePath:     initreq.Path,
		Root:             s.Root,
	}
	if initreq.HasRefs {
		total, err = upload.Partial(absolutePath, backupSuffix, stream, chunkRepo, chunkIndex)
	} else {
		total, err = upload.Full(absolutePath, stream, chunkRepo, chunkIndex)
	}

	if err != nil {
		return err
	}

	if err := stream.Send(&pb.UploadRes{Written: uint64(total)}); err != nil {
		return raiseInternalError("fail sending result uploading message: %w", err)
	}

	return nil
}

func (s Server) dropIndex(relativePath string) error {
	chunkIndex := index.NewChunkIndex()
	if err := s.ChunkIndexRepo.Get(relativePath, &chunkIndex); err != nil {
		return fmt.Errorf("fail getting ChunkIndex of file: %w", err)
	}

	if err := s.ChunkIndexRepo.Drop(relativePath); err != nil {
		return fmt.Errorf("fail dropping ChunkIndex: %w", err)
	}

	pathIndex, ok := s.PathIndex.Get(relativePath)
	if !ok {
		return fmt.Errorf("path index for %+q does not exist", relativePath)
	}

	_, unlock := s.PathIndex.Lock(pathIndex)
	defer unlock()
	for checksum, entry := range chunkIndex.Data {
		for _, ref := range entry.Refs {
			s.GlobalChunkIndex.Drop(checksum, index.GlobalChunkRef{pathIndex, ref})
		}
	}

	s.PathIndex.Drop(relativePath)
	return nil
}

// Remove removes designated file. If a folder is designated then it must empty.
func (s Server) Remove(ctx context.Context, req *pb.RemoveReq) (*pb.RemoveRes, error) {
	stat, err := os.Stat(path.Join(s.Root, req.Path))
	switch {
	case os.IsNotExist(err):
		// It's done for the case when a request to delete a file comes after a request to delete
		// its parent folder.
		s.dropIndex(req.Path)
		return &pb.RemoveRes{Kind: pb.Result_OK}, nil
	case err != nil:
		return nil, raiseInternalError("fail getting stat to remove a file", err)
	}

	// Let's don't lock s.PathIndex without reason.
	if !stat.IsDir() {
		if err := s.dropIndex(req.Path); err != nil {
			log.Println("fail dropping index of file before removing", err)
		}
	}

	if err := os.Remove(path.Join(s.Root, req.Path)); err != nil {
		patherr := err.(*os.PathError)
		if osErr, ok := patherr.Err.(syscall.Errno); ok && osErr == syscall.ENOTEMPTY {
			return nil, status.Error(codes.FailedPrecondition, "The directory is not empty")
		}
		return nil, raiseInternalError("fail removing "+req.Path, err)
	}

	return &pb.RemoveRes{Kind: pb.Result_OK}, nil
}

func (s Server) FileChecksum(ctx context.Context, req *pb.FileChecksumReq) (*pb.FileChecksumRes, error) {
	var (
		absolutePath = path.Join(s.Root, req.Path)
	)

	_, err := os.Stat(absolutePath)
	switch {
	case os.IsNotExist(err):
		return nil, status.Error(codes.NotFound, "file not found")
	case err != nil:
		return nil, raiseInternalError("fail getting stat of the requested file", err)
	}

	index, ok := s.PathIndex.Get(req.Path)
	if !ok {
		// at this point we could write a warning message and go on without locking if the file is not
		// indexed but it could lead to concurrency issues.
		return nil, status.Error(codes.Internal, "Internal error")
	}

	_, unlock := s.PathIndex.RLock(index)
	if unlock == nil {
		log.Printf("fail getting read lock on %+q (path index = %d)\n", req.Path, index)
		return nil, status.Error(codes.Internal, "Internal error")
	}

	defer unlock()

	checksum := [16]byte{}
	if err := hash.MD5File(absolutePath, checksum[:]); err != nil {
		return nil, raiseInternalError("fail getting md5 of file", err)
	}

	high, low := hash.MD5littleEndian(checksum)
	return &pb.FileChecksumRes{High: high, Low: low}, nil
}

// Checksum sends batches of ChunkID of chunks of data of the requested file
// into stream. Strong sum of a chunk is MD5 (16 bytes) splitted into two
// uint64 values.
func (s Server) Checksum(req *pb.ChecksumReq, stream pb.FileRepo_ChecksumServer) error {
	var (
		filepath = path.Join(s.Root, req.Path)
		sumsBuf  = checksumbuffer{make([]*pb.ChunkID, 0), stream, checksumLimit}
	)

	_, err := os.Stat(filepath)
	switch {
	case os.IsNotExist(err):
		return status.Error(codes.NotFound, "file not found")
	case err != nil:
		return raiseInternalError("fail getting stat of the requested file", err)
	}

	fd, err := os.Open(filepath + backupSuffix)
	if err != nil {
		fd, err = os.Open(filepath)
		if err != nil {
			return raiseInternalError("fail openning requested file", err)
		}
	}

	defer fd.Close()
	// TODO: should be done more nice for the sake of RAM.
	index := index.NewChunkIndex()
	if err := index.Compute(fd); err != nil {
		return raiseInternalError("fail building index", err)
	}

	for strongHash, entry := range index.Data {
		high, low := hash.MD5littleEndian(strongHash)
		for _, ref := range entry.Refs {
			chunkid := &pb.ChunkID{
				Ref:        ref,
				Weak:       entry.Weak,
				StrongHigh: high,
				StrongLow:  low,
			}

			if err := sumsBuf.push(chunkid); err != nil {
				return raiseInternalError("fail push a chunk to the stream", err)
			}
		}
	}

	if err := sumsBuf.flush(); err != nil {
		return raiseInternalError("fail sending chunks checksums to the stream", err)
	}

	return nil
}

func (s *Server) Init(ctx context.Context) error {
	if err := s.ChunkIndexRepo.Init(); err != nil {
		return fmt.Errorf("fail initializing chunkIndexRepo: %w", err)
	}

	lis, err := net.Listen("tcp", fmt.Sprintf("localhost:%d", s.Port))
	if err != nil {
		return fmt.Errorf("fail listenning: %w", err)
	}

	cred := new(credentials.TransportCredentials)
	if err := tls.Credentials(cred, s.ServerPem, s.ServerKey, s.Cacert); err != nil {
		return fmt.Errorf("fail getting TLS credentials: %w", err)
	}

	s.GlobalChunkIndex.Init()
	grpcserver := grpc.NewServer(grpc.Creds(*cred))
	pb.RegisterFileRepoServer(grpcserver, s)
	go func() {
		<-ctx.Done()
		grpcserver.GracefulStop()
		log.Println("GRPC server is stopped.")
	}()

	if err := grpcserver.Serve(lis); err != nil {
		return fmt.Errorf("fail starting GRPC server: %w", err)
	}

	return nil
}
