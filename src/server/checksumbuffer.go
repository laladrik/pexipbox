package server

import (
	"fmt"

	pb "gitlab.com/laladrik/pexipbox/proto"
)

type checksumbuffer struct {
	buf    []*pb.ChunkID
	stream pb.FileRepo_ChecksumServer
	limit  int
}

func (c *checksumbuffer) push(item *pb.ChunkID) error {
	c.buf = append(c.buf, item)
	if len(c.buf) == c.limit {
		return c.flush()
	}

	return nil
}

func (c *checksumbuffer) flush() error {
	if len(c.buf) == 0 {
		return nil
	}

	if err := c.stream.Send(&pb.ChecksumRes{Batch: c.buf}); err != nil {
		return fmt.Errorf("fail sending checksums: %w", err)
	}

	c.buf = make([]*pb.ChunkID, 0)
	return nil
}
