// The file implements an observer looks after file system events. It passes them to pool of workers
// which handle them. The workerpool holds events until a worker grabs it.
// To edit the dispatch go to workerpool.go
// To edit handling of an event to worker.go
package fs

import (
	"context"
	"fmt"
	"os"

	"gitlab.com/laladrik/pexipbox/fsutils"
	plog "gitlab.com/laladrik/pexipbox/log"

	"github.com/fsnotify/fsnotify"
)

var log = plog.New("fs")

type FsObserve struct {
	Root string
}

func (f FsObserve) files(ctx context.Context, eventConsumer *WorkerPool) error {
	return fsutils.Walk(ctx, f.Root, func(path string) error {
		eventConsumer.OnEvent(fsnotify.Event{
			Name: path,
			Op:   fsnotify.Create,
		})

		stat, err := os.Stat(path)
		if err != nil {
			return fmt.Errorf("fail getting file stat: %w", err)
		}

		if !stat.IsDir() && stat.Size() > 0 {
			eventConsumer.OnEvent(fsnotify.Event{
				Name: path,
				Op:   fsnotify.Write,
			})
		}

		return nil
	})
}

// Run does:
// - launches eventConsumer (consumes and handles file system events);
// - send the eventConsumer `create` and `write` event of existent files;
// - looks after new file system events from the f.Root folder. Every event is sent to the
// eventConsumer. If a new folder is create then it looks after it as well.
//
// If ctx context is canceled then it drops new file system events and waits when the eventConsumer
// finishes.
func (f FsObserve) Run(ctx context.Context, eventConsumer *WorkerPool) error {
	eventReceiverDone := make(chan struct{})
	defer close(eventReceiverDone)
	go func() {
		defer func() {
			log.Println("consuming events is stopped")
			eventReceiverDone <- struct{}{}
		}()
		eventConsumer.Run(ctx)
	}()

	f.files(ctx, eventConsumer)
	watcher, err := fsnotify.NewWatcher()
	if err != nil {
		return fmt.Errorf("fail creating a watcher: %w", err)
	}

	if err := watcher.Add(f.Root); err != nil {
		return fmt.Errorf("fail watching folder %+q: %w", f.Root, err)
	}

	defer func() {
		if err := watcher.Close(); err != nil {
			log.Println("fail closing the watcher of the root directory", err)
		}
	}()

	go func() {
		for {
			select {
			case fserr := <-watcher.Errors:
				log.Println("new fs error", fserr.Error())
			case fsevent := <-watcher.Events:
				log.Println("new fs event on file", fsevent.String())
				if fsevent.Op&fsnotify.Create == fsnotify.Create {
					stat, err := os.Stat(fsevent.Name)
					if err != nil {
						log.Printf("fail getting stat of %+q: %s\n", fsevent.Name, err)
						continue
					}

					if stat.IsDir() {
						if err := watcher.Add(fsevent.Name); err != nil {
							log.Printf("fail watching %+q: %s\n", fsevent.Name, err)
							continue
						}
					}
				}

				eventConsumer.OnEvent(fsevent)
			case <-ctx.Done():
				log.Println("receiving filesystem notifications is stopped")
				return
			}
		}
	}()

	<-eventReceiverDone
	log.Println("file observation is stopped")
	return nil
}
