package fs

import (
	"sync"

	"github.com/fsnotify/fsnotify"
)

type filepath = string

type jobMap struct {
	inner map[filepath]*worker
	lock  sync.Mutex
}

func (s *jobMap) assign(key filepath, value *worker) {
	s.lock.Lock()
	defer s.lock.Unlock()
	s.inner[key] = value
}

func (s *jobMap) unassign(key filepath) {
	s.lock.Lock()
	defer s.lock.Unlock()
	delete(s.inner, key)
}

func (s *jobMap) tryEscalate(ev fsnotify.Event) bool {
	s.lock.Lock()
	defer s.lock.Unlock()
	if worker, ok := s.inner[ev.Name]; ok {
		worker.private.Push(ev)
		return true
	}

	return false
}
