// Implementation of handling messages: consuming messages in a proper way searching unsynchronized
// chunks of files or files themselves and sends proper messages to the server. To edit the sending
// client/common.go
package fs

import (
	"bytes"
	"context"
	"crypto/md5"
	"fmt"
	"os"
	"strings"
	"syscall"

	"github.com/fsnotify/fsnotify"
	"gitlab.com/laladrik/pexipbox/client"
	"gitlab.com/laladrik/pexipbox/collections"
	"gitlab.com/laladrik/pexipbox/diff"
	"gitlab.com/laladrik/pexipbox/hash"
)

type worker struct {
	root      string
	transport *client.Client
	private   *collections.EventQueue
}

// It's more efficient to change CWD rather than clipping prefix every
// time but the inotify wrapper provides only an absolute path even if
// a watcher looks after a CWD
func (w worker) trimPath(in string) string {
	return strings.TrimPrefix(in, w.root)
}

func (w worker) create(ctx context.Context, event fsnotify.Event) error {
	stat, err := os.Stat(event.Name)
	if err != nil {
		return fmt.Errorf("fail getting stat of created file: %w", err)
	}

	path := w.trimPath(event.Name)
	if err := w.transport.Create(ctx, path, stat.IsDir(), stat.ModTime()); err != nil {
		return fmt.Errorf("fail handling creating a file: %w", err)
	}

	return nil
}

func (w worker) verify(ctx context.Context, filepath string) error {
	checksum := [md5.Size]byte{}
	if err := hash.MD5File(filepath, checksum[:]); err != nil {
		return err
	}

	high, low := hash.MD5littleEndian(checksum)
	remoteHigh, remoteLow := uint64(0), uint64(0)
	if err := w.transport.FileChecksum(ctx, filepath, &remoteHigh, &remoteLow); err != nil {
		return err
	}

	if remoteHigh == high && remoteLow == low {
		return nil
	}

	return fmt.Errorf("verification failed")
}

// Downloads an array of weak and strong checksums from the server of a remote copy of file.
// Computes an array of weak checksums of the local copy of the file shifting by 1 byte. Finds
// difference and sends it stream of messages. A message consists of data or a reference to chunk
// the remote copy has.
func (w worker) write(ctx context.Context, event fsnotify.Event) error {
	if err := w.verify(ctx, event.Name); err == nil {
		return nil
	}

	fd, err := os.Open(event.Name)
	if err != nil {
		return fmt.Errorf("fail openning a created file: %w", err)
	}

	defer fd.Close()
	stat, err := fd.Stat()
	if err != nil {
		return fmt.Errorf("fail getting stat of a file: %w", err)
	}

	size := int(stat.Size())
	data, err := syscall.Mmap(int(fd.Fd()), 0, size, syscall.PROT_READ, syscall.MAP_PRIVATE)
	if err != nil {
		return fmt.Errorf("fail creating a memory map to a file: %w", err)
	}

	defer syscall.Munmap(data)
	path := w.trimPath(event.Name)
	checksums := collections.ChecksumIndex{}
	checksumStat := new(client.ChecksumStat)
	if err := w.transport.Checksum(ctx, path, &checksums, checksumStat); err != nil {
		return fmt.Errorf("fail getting checksums of remote copy of the file: %w", err)
	}

	log.Printf(
		"%d checksums are received in %d batches\n",
		checksumStat.ChecksumCount,
		checksumStat.BatchCount,
	)

	fileDiff := diff.Diff{Items: make([]diff.DiffItem, 0)}
	localBuf := bytes.NewBuffer(data)
	if err := diff.Compute(&fileDiff, localBuf, checksums); err != nil {
		return fmt.Errorf("fail computing difference between local and remote copies of a file: %w", err)
	}

	uploadStat := new(client.UploadStat)
	if err = w.transport.Upload(ctx, uploadStat, path, data, fileDiff); err != nil {
		return fmt.Errorf("fail handling writing a file: %w", err)
	}

	log.Printf("%d bytes are sent, %d bytes are synchronized\n", uploadStat.Sent, uploadStat.Uploaded)
	return w.verify(ctx, event.Name)

}

// Consumes events from private and public channels. Prerers events from the
// private channel as the events are for the file the last processed event for.
// If an event is consumed from public channel then the worker registers
// in currentJobs and awknowledges its receiving of the event.
//
// If ctx context is canceled:
// - stops receiving new file system events.
// - waits finishing of current GRPC request.
func (w *worker) run(
	ctx context.Context,
	public <-chan fsnotify.Event,
	awk chan<- struct{},
	currentJobs *jobMap,
) {
	for {
		select {
		case <-w.private.Wait():
			for {
				ev := fsnotify.Event{}
				if err := w.private.Pop(&ev); err != nil {
					break
				}

				select {
				case <-ctx.Done():
					break
				default:
					func() {
						currentJobs.assign(ev.Name, w)
						defer currentJobs.unassign(ev.Name)
						w.process(ctx, ev)
					}()
				}
			}

		case ev := <-public:
			func() {
				currentJobs.assign(ev.Name, w)
				defer currentJobs.unassign(ev.Name)
				awk <- struct{}{}
				w.process(ctx, ev)
			}()
		case <-ctx.Done():
			return
		}

	}
}

func (w *worker) process(ctx context.Context, ev fsnotify.Event) {
	log.Println("process event", ev.String())
	var err error
	switch {
	case ev.Op&fsnotify.Create == fsnotify.Create:
		err = w.create(ctx, ev)
	case ev.Op&fsnotify.Remove == fsnotify.Remove:
		err = w.transport.Remove(ctx, w.trimPath(ev.Name))
	case ev.Op&fsnotify.Write == fsnotify.Write:
		err = w.write(ctx, ev)
	}

	if err != nil {
		log.Println(err)
	}
}
