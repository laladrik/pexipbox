package fs

import (
	"context"
	"sync"

	"github.com/fsnotify/fsnotify"
	"gitlab.com/laladrik/pexipbox/client"
	"gitlab.com/laladrik/pexipbox/collections"
)

type WorkerPool struct {
	size      int
	input     collections.EventQueue
	root      string
	transport *client.Client
}

// Run workers in their own threads. WorkerPool consumes file system events
// from the input queue then routes them. An event is routed to the private
// channel of a certain worker if the worker works on an event of the same
// file. An event is routed the public channel then WorkerPool waits until
// a worker registers in currentJobs and awknowledges it.
//
// If ctx context is canceled:
// - workerPool stops consuming new file system events and producing new jobs;
// - waits until the workers stop;
// - closes channels;
//
// TODO: figure out a way to cancel current work on a file if a new event
// appears.
func (p *WorkerPool) Run(ctx context.Context) {
	var (
		currentJobs = jobMap{
			inner: make(map[filepath]*worker),
		}
		workersWait = sync.WaitGroup{}
		public      = make(chan fsnotify.Event)
		awk         = make(chan struct{})
	)

	defer close(public)
	defer close(awk)
	workersWait.Add(p.size)
	defer workersWait.Wait()
	for i := 0; i < p.size; i++ {
		go func(i int) {
			defer workersWait.Done()
			w := worker{
				p.root,
				p.transport,
				collections.NewEventQueue(),
			}

			defer w.private.Fin()
			w.run(ctx, public, awk, &currentJobs)
		}(i)
	}

	for {
		select {
		case <-p.input.Wait():
			for {
				ev := fsnotify.Event{}
				if err := p.input.Pop(&ev); err != nil {
					break
				}

				if !currentJobs.tryEscalate(ev) {
					public <- ev
					<-awk
				}
			}
		case <-ctx.Done():
			log.Println("stopping pool")
			return
		}
	}

}

func (p *WorkerPool) OnEvent(ev fsnotify.Event) {
	p.input.Push(ev)
}

func NewWorkerPool(size int, root string, transport *client.Client) WorkerPool {
	return WorkerPool{
		transport: transport,
		root:      root,
		size:      size,
		input:     *collections.NewEventQueue(),
	}
}

func (p *WorkerPool) Fin() {
	p.input.Wait()
}
