module gitlab.com/laladrik/pexipbox

go 1.14

require (
	github.com/fsnotify/fsnotify v1.4.9
	github.com/golang/protobuf v1.4.3
	github.com/stretchr/testify v1.5.1
	github.com/vektra/mockery/v2 v2.7.4 // indirect
	google.golang.org/grpc v1.36.1
)
