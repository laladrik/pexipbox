package index

import (
	"sync"
)

// PathIndex stores indexes of relative paths to files. It is designated for two purposes:
// - GlobalChunkIndex as it can have a lot of entries to the save file. If every its entry has a copy
// to the same path it's spending RAM for nothing. The structure prevents it. An entry in PathIndex
// must be removed only when there is no references in GlobalChunkIndex to the entry.
// - Methods Lock and RLock are used to keep concurrent-safe access to files. (Sorf of flock but
// inside a process.)
type PathIndex struct {
	lock      *sync.RWMutex
	indexes   map[string]int
	paths     map[int]string
	pathLocks map[int]*sync.RWMutex
	indexseq  int
}

func (p *PathIndex) Lock(index int) (string, func()) {
	p.lock.RLock()
	defer p.lock.RUnlock()
	if ret, ok := p.pathLocks[index]; ok {
		ret.Lock()
		return p.paths[index], ret.Unlock
	}
	return "", nil
}

func (p *PathIndex) RLock(index int) (string, func()) {
	p.lock.RLock()
	defer p.lock.RUnlock()
	if ret, ok := p.pathLocks[index]; ok {
		ret.RLock()
		return p.paths[index], ret.RUnlock
	}
	return "", nil
}

func (p *PathIndex) Path(index int) (string, bool) {
	p.lock.RLock()
	defer p.lock.RUnlock()
	ret, ok := p.paths[index]
	return ret, ok
}

func (p *PathIndex) Set(relativePath string) int {
	p.lock.Lock()
	defer p.lock.Unlock()
	if ret, ok := p.indexes[relativePath]; ok {
		return ret
	}

	p.indexes[relativePath] = p.indexseq
	p.paths[p.indexseq] = relativePath
	p.pathLocks[p.indexseq] = new(sync.RWMutex)
	p.indexseq++
	return p.indexes[relativePath]
}

func (p *PathIndex) Get(relativePath string) (int, bool) {
	p.lock.RLock()
	defer p.lock.RUnlock()
	ret, ok := p.indexes[relativePath]
	return ret, ok
}

func (p *PathIndex) Drop(relativePath string) {
	p.lock.Lock()
	defer p.lock.Unlock()
	index := p.indexes[relativePath]
	delete(p.paths, index)
	delete(p.indexes, relativePath)
}

func NewPathIndex() PathIndex {
	return PathIndex{
		new(sync.RWMutex),
		make(map[string]int),
		make(map[int]string),
		make(map[int]*sync.RWMutex),
		0,
	}
}
