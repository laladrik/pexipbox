package index

import (
	"crypto/md5"
	"io/ioutil"
	"os"
	"testing"

	"github.com/stretchr/testify/require"
)

func TestChunkIndexRepo(t *testing.T) {
	t.Run("serialization_deserialization", func(t *testing.T) {
		var (
			indexOut          = NewChunkIndex()
			require           = require.New(t)
			indexRelativePath = "gamesaves/0001.dat"
			indexIn           = ChunkIndex{
				Data: chunkTable{
					md5.Sum([]byte{1, 2, 3}): ChunkIndexEntry{10, []uint64{0, 1, 2, 3, 4}},
					md5.Sum([]byte{2, 4, 3}): ChunkIndexEntry{12, []uint64{5, 6}},
				},
			}
		)

		dir, err := ioutil.TempDir("", "")
		require.NoError(err)
		defer os.RemoveAll(dir)
		repo := ChunkIndexRepo{Root: dir}
		require.NoError(repo.Init())
		require.NoError(repo.Set(indexRelativePath, indexIn))
		require.NoError(repo.Get(indexRelativePath, &indexOut))
		require.Equal(indexOut, indexIn)
	})
}
