package index

import "sync"

type GlobalChunkRef = struct {
	Pathindex int
	Chunkref  uint64
}

type refSet = map[GlobalChunkRef]struct{}

var void = struct{}{}

type globalChunkIndexEntry = struct {
	lock *sync.RWMutex
	refs refSet
}

// GlobalChunkIndex allows to find a chunk by a checksum across multiple files.
//
// TODO: use B-Tree instead of a map as the index supposed to store a lot of entries.
type GlobalChunkIndex struct {
	lock    *sync.RWMutex
	entries map[Checksum]globalChunkIndexEntry
}

func (g *GlobalChunkIndex) getEntry(key Checksum) (globalChunkIndexEntry, bool) {
	g.lock.RLock()
	defer g.lock.RUnlock()
	ret, ok := g.entries[key]
	return ret, ok
}

func (g *GlobalChunkIndex) Get(sum Checksum, out *GlobalChunkRef) bool {
	if entry, ok := g.getEntry(sum); ok {
		entry.lock.RLock()
		defer entry.lock.RUnlock()
		for item := range entry.refs {
			*out = item
			return true
		}
	}

	return false
}

func (g *GlobalChunkIndex) Init() {
	g.lock = new(sync.RWMutex)
	g.entries = make(map[Checksum]globalChunkIndexEntry)
}

func (g *GlobalChunkIndex) Add(sum Checksum, ref GlobalChunkRef) {
	if entry, ok := g.getEntry(sum); ok {
		entry.lock.Lock()
		entry.refs[ref] = void
		entry.lock.Unlock()
		return
	}

	g.lock.Lock()
	g.entries[sum] = globalChunkIndexEntry{new(sync.RWMutex), refSet{ref: void}}
	g.lock.Unlock()
}

func (g *GlobalChunkIndex) Drop(sum Checksum, ref GlobalChunkRef) {
	entry, ok := g.getEntry(sum)
	if !ok {
		return
	}

	entry.lock.Lock()
	delete(entry.refs, ref)
	entry.lock.Unlock()

	g.lock.Lock()
	if len(entry.refs) == 0 {
		delete(g.entries, sum)
	}
	g.lock.Unlock()
}
