package index

import (
	"bufio"
	"bytes"
	"crypto/md5"
	"encoding/binary"
	"fmt"
	"io"
	"os"
	"path"
	"sync"

	"gitlab.com/laladrik/pexipbox/hash"
)

type (
	Checksum  = [strongChecksumLen]byte
	readState = byte
)

const (
	strongChecksumLen = md5.Size
	weakChecksumLen   = 4
	chunkrefLen       = 8

	readChecksumsState int = iota
	readRefsState
)

var (
	littleEndian     = binary.LittleEndian
	ErrEmpty         = fmt.Errorf("empty file")
	ErrNotReachable  = fmt.Errorf("not reachable")
	ErrTooManyChunks = fmt.Errorf("too many chunks")
)

type ChunkIndexEntry = struct {
	Weak hash.Sum
	Refs []uint64
}

// ChunkIndexRepo provides concurrent-safe access to ChunkIndex of a certain file.
type ChunkIndexRepo struct {
	lock       *sync.Mutex
	Root       string
	indexLocks map[string]*sync.RWMutex
}

// TODO: must make r.Root consisting only actual meta data.
func (r *ChunkIndexRepo) Init() error {
	_, err := os.Stat(r.Root)
	switch {
	case os.IsNotExist(err):
		if err := os.Mkdir(r.Root, 0755); err != nil {
			return err
		}
	case err != nil:
		return err
	}

	r.lock = new(sync.Mutex)
	r.indexLocks = make(map[string]*sync.RWMutex)
	return nil
}

func checkSums(data []byte) (Checksum, hash.Sum) {
	strong := Checksum{}
	copy(strong[:], data)
	weak := hash.Sum(0)
	binary.Read(
		bytes.NewReader(data[strongChecksumLen:]),
		binary.LittleEndian,
		&weak,
	)
	return strong, weak
}

func readChecksums(reader io.Reader) (Checksum, hash.Sum, error) {
	var (
		none          = [strongChecksumLen]byte{}
		checksumSlice = make([]byte, strongChecksumLen+weakChecksumLen)
		checksumLen   = strongChecksumLen + weakChecksumLen
	)

	n, err := reader.Read(checksumSlice)
	switch {
	case err != nil:
		return none, 0, err
	case n != checksumLen:
		return none, 0, fmt.Errorf("length of slice is %v but should be %v", n, checksumLen)
	}

	strong, weak := checkSums(checksumSlice)
	return strong, weak, nil
}

func (r *ChunkIndexRepo) getLock(relativePath string) (*sync.RWMutex, error) {
	r.lock.Lock()
	defer r.lock.Unlock()
	indexLock, ok := r.indexLocks[relativePath]
	if !ok {
		if _, err := os.Stat(relativePath); err != nil {
			return nil, ErrNotReachable
		}
		r.indexLocks[relativePath] = new(sync.RWMutex)
		indexLock = r.indexLocks[relativePath]
	}

	return indexLock, nil
}

func (r *ChunkIndexRepo) getLockOrCreate(relativePath string) *sync.RWMutex {
	r.lock.Lock()
	defer r.lock.Unlock()
	indexLock, ok := r.indexLocks[relativePath]
	if !ok {
		r.indexLocks[relativePath] = new(sync.RWMutex)
		indexLock = r.indexLocks[relativePath]
	}

	return indexLock
}

// Get reads checksums and chunk references from the file named by relativePath into out.
//
// A file consists of blocks. A block has such structure:
// - <16 bytes strong checksum>
// - <4 bytes weak checksum>
// - <4 bytes is number of the following references>
// - <repeated chunks references of 8 bytes each one>
func (r *ChunkIndexRepo) Get(relativePath string, out *ChunkIndex) error {
	indexLock, err := r.getLock(relativePath)
	if err != nil {
		return err
	}

	indexLock.RLock()
	defer indexLock.RUnlock()
	indexfile, err := os.Open(path.Join(r.Root, relativePath))
	if err != nil {
		return fmt.Errorf("fail openning chunk index file: %w", err)
	}

	reader := bufio.NewReader(indexfile)
	state := readChecksumsState
	strong, weak, err := readChecksums(reader)
	if err != nil {
		return fmt.Errorf("fail reading checksums: %w", err)
	}

	state = readRefsState
	entry := ChunkIndexEntry{Weak: weak, Refs: nil}
	for {
		switch state {
		case readChecksumsState:
			out.Data[strong] = entry
			strong, weak, err = readChecksums(reader)
			switch {
			case err == io.EOF:
				return nil
			case err != nil:
				return fmt.Errorf("fail reading checksums: %w", err)
			}

			entry = ChunkIndexEntry{Weak: weak, Refs: nil}
			state = readRefsState
		case readRefsState:
			refnum := uint32(0)
			if err := binary.Read(reader, littleEndian, &refnum); err != nil {
				return fmt.Errorf("fail reading number of references: %w", err)
			}

			entry.Refs = make([]uint64, refnum)
			for i := uint32(0); i < refnum; i++ {
				if err := binary.Read(reader, littleEndian, &entry.Refs[i]); err != nil {
					return fmt.Errorf("fail reading references: %w", err)
				}
			}

			state = readChecksumsState
		default:
			panic("unexpected state")
		}
	}
}

// Set saves index into the file designated by the relativePath.
//
// A file consists of blocks. A block has such structure:
// - <16 bytes strong checksum>
// - <4 bytes weak checksum>
// - <4 bytes is number of the following references>
// - <repeated chunks references of 8 bytes each one>
func (r *ChunkIndexRepo) Set(relativePath string, index ChunkIndex) error {
	const (
		maxInt32 = int(^uint32(0) >> 1)
	)

	indexLock := r.getLockOrCreate(relativePath)
	indexLock.Lock()
	defer indexLock.Unlock()
	absolutePath := path.Join(r.Root, relativePath)
	if err := os.MkdirAll(path.Dir(absolutePath), 0755); err != nil {
		return fmt.Errorf("fail creating directory: %w", err)
	}

	indexfile, err := os.OpenFile(absolutePath, os.O_CREATE|os.O_WRONLY|os.O_TRUNC, 0644)
	if err != nil {
		return fmt.Errorf("fail openning chunk index file: %w", err)
	}

	buf := bufio.NewWriter(indexfile)
	for checksum, entry := range index.Data {
		if _, err := buf.Write(checksum[:]); err != nil {
			return fmt.Errorf("fail writing a checksum: %w", err)
		}

		if err := binary.Write(buf, littleEndian, entry.Weak); err != nil {
			return fmt.Errorf("fail writing a chunk reference: %w", err)
		}

		refCount := len(entry.Refs)
		if refCount > maxInt32 {
			return ErrTooManyChunks
		}

		if err := binary.Write(buf, littleEndian, uint32(refCount)); err != nil {
			return fmt.Errorf("fail writing a chunk reference: %w", err)
		}

		for _, chunkref := range entry.Refs {
			if err := binary.Write(buf, littleEndian, chunkref); err != nil {
				return fmt.Errorf("fail writing a chunk reference: %w", err)
			}
		}
	}

	if err := buf.Flush(); err != nil {
		return fmt.Errorf("fail flushing a buffer: %w", err)
	}

	return nil
}

func (r *ChunkIndexRepo) Recompute(relativePath string, newIndex, oldIndex *ChunkIndex) error {
	absolutePath := path.Join(r.Root, relativePath)
	fd, err := os.Open(absolutePath)
	if err != nil {
		return fmt.Errorf("fail openning a file to index it: %w", err)
	}

	defer fd.Close()
	if err := newIndex.Compute(fd); err != nil {
		return fmt.Errorf("fail building index: %w", err)
	}

	if err := r.Get(relativePath, oldIndex); err != nil {
		return fmt.Errorf("fail retrieving index: %w", err)
	}

	if err := r.Set(relativePath, *newIndex); err != nil {
		return fmt.Errorf("fail saving index: %w", err)
	}

	return nil
}

func (r *ChunkIndexRepo) Drop(relativePath string) error {
	r.lock.Lock()
	defer r.lock.Unlock()
	delete(r.indexLocks, relativePath)
	return os.Remove(path.Join(r.Root, relativePath))
}
