package index

import (
	"crypto/md5"
	"fmt"
	"io"
	"os"

	"gitlab.com/laladrik/pexipbox/hash"
)

const iosize = 4096

type chunkTable = map[Checksum]ChunkIndexEntry

// ChunkIndex is used to get a chunk reference by its checksum. It stores checksum against a
// structure of weak checksum and an array of references sorted in ascending order.
type ChunkIndex struct {
	Data chunkTable
}

func (c *ChunkIndex) Compute(fd *os.File) error {
	var (
		buf = make([]byte, iosize)
	)

	for i := uint64(0); ; i++ {
		n, err := fd.Read(buf)
		switch {
		case err == io.EOF:
			return nil
		case err != nil:
			return fmt.Errorf("fail reading a file: %w", err)
		}

		strongHash := md5.Sum(buf[:n])
		entry, ok := c.Data[strongHash]
		weak, refs := entry.Weak, entry.Refs
		if !ok {
			weak = hash.NewRolling(buf[:n]).Sum()
			refs = make([]uint64, 0, 1)
		}

		c.Data[strongHash] = ChunkIndexEntry{weak, append(refs, i)}
	}
}

// Diff computes difference between c and that. If a checksum appears in c.Data but no in that.Data
// then it is added to removals. If a checksum does not appear in c.Data but appears in that.Data
// then it's added to additions.
// Returns an error only if onAddition or onRemoval returns one.
func (c ChunkIndex) Diff(that ChunkIndex, onAddition, onRemoval func(sum Checksum, ref uint64) error) error {
	if err := getdiff(c.Data, that.Data, onAddition, onRemoval); err != nil {
		return err
	}

	if err := getdiff(that.Data, c.Data, onRemoval, onAddition); err != nil {
		return err
	}

	return nil
}

func (c *ChunkIndex) Init(absolutePath string) error {
	fd, err := os.Open(absolutePath)
	if err != nil {
		return err
	}

	defer fd.Close()
	return c.Compute(fd)
}

func NewChunkIndex() ChunkIndex {
	return ChunkIndex{make(chunkTable)}
}

// TODO: perhaps there is a way to compute it faster than O(n) where n is number of chunk references.
func getdiff(left, right chunkTable, onAddition, onRemoval func(Checksum, uint64) error) error {
	for sum, leftEntry := range left {
		rightEntry, ok := right[sum]
		if !ok {
			for _, ref := range leftEntry.Refs {
				if err := onRemoval(sum, ref); err != nil {
					return err
				}
			}

			continue
		}

		for leftRefCount, rightRefCount := 0, 0; leftRefCount < len(leftEntry.Refs) && rightRefCount < len(rightEntry.Refs); {
			leftRef := leftEntry.Refs[leftRefCount]
			rightRef := rightEntry.Refs[rightRefCount]

			switch {
			case leftRef < rightRef:
				if err := onRemoval(sum, leftRef); err != nil {
					return err
				}
				leftRefCount++
			case leftRef > rightRef:
				if err := onAddition(sum, rightRef); err != nil {
					return err
				}
				rightRefCount++
			default:
				leftRefCount++
				rightRefCount++
			}
		}
	}

	return nil
}
