package tls

import (
	"crypto/tls"
	"crypto/x509"
	"io/ioutil"
	"log"

	"google.golang.org/grpc/credentials"
)

func Credentials(out *credentials.TransportCredentials, pem, key, cacert string) error {
	cert, err := tls.LoadX509KeyPair(pem, key)
	if err != nil {
		log.Fatalf("Failed to load client certificate and key. %s.", err)
	}

	trustedCert, err := ioutil.ReadFile(cacert)
	if err != nil {
		log.Fatalf("Failed to load trusted certificate. %s.", err)
	}

	certPool := x509.NewCertPool()
	if !certPool.AppendCertsFromPEM(trustedCert) {
		log.Fatalf("Failed to append trusted certificate to certificate pool. %s.", err)
	}

	*out = credentials.NewTLS(&tls.Config{
		Certificates: []tls.Certificate{cert},
		RootCAs:      certPool,
		ClientCAs:    certPool,
		MinVersion:   tls.VersionTLS13,
		MaxVersion:   tls.VersionTLS13,
	})

	return nil
}
