package hash

import "testing"

func TestPush(t *testing.T) {
	data := []byte{0x77, 0x67, 0x8a, 0xf8, 0x27, 0x5f, 0xfe, 0x09, 0x00, 0x00, 0xff, 0xff, 0xa4}
	t.Run("success", func(t *testing.T) {
		actual := NewRolling(data)
		actual.Push(0x65)
		expected := NewRolling(append(data[1:], 0x65))
		if actual.Sum() != expected.Sum() {
			t.Fatalf("rolling sums mismatch: expected = %d, actual = %d", expected.Sum(), actual.Sum())
		}
	})

	t.Run("fail", func(t *testing.T) {
		actual := NewRolling(data)
		actual.Push(0x65)
		notExpected := NewRolling(append(data[1:], 0x80))
		if actual.Sum() == notExpected.Sum() {
			t.Fatalf(
				"rolling sums matches but must not: not expected = %d, actual = %d",
				notExpected.Sum(),
				actual.Sum(),
			)
		}
	})
}
