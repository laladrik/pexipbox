package hash

import (
	"bytes"
	"crypto/md5"
	"encoding/binary"
	"io"
	"os"
)

func MD5File(filepath string, out []byte) error {
	hash := md5.New()
	fd, err := os.Open(filepath)
	if err != nil {
		return err
	}

	if _, err := io.Copy(hash, fd); err != nil {
		return err
	}

	copy(out[:], hash.Sum(nil))
	return nil
}

func MD5littleEndian(hash [md5.Size]byte) (high uint64, low uint64) {
	binary.Read(bytes.NewReader(hash[:8]), binary.LittleEndian, &high)
	binary.Read(bytes.NewReader(hash[8:]), binary.LittleEndian, &low)
	return high, low
}
