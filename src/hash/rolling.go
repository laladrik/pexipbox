package hash

type Sum = uint32

const (
	mod  = 65536
	nmax = 4096
)

// Rolling implements https://rsync.samba.org/tech_report/node3.html. It is a
// FIFO of fixed size designated by an initial array's length.
type Rolling struct {
	data []byte
	asum Sum
	bsum Sum
}

// Push pushes value to the inner data and recomputes asum and bsum.
func (r *Rolling) Push(value byte) {
	out := Sum(r.data[0])
	r.asum = (r.asum - out + Sum(value)) % mod

	r.bsum = (r.bsum - Sum(len(r.data)-1+1)*out) + r.asum
	r.bsum %= mod
	r.data = append(r.data[1:], value)
}

func (r Rolling) Sum() Sum {
	return r.asum + mod*r.bsum
}

func RollingSum(data []byte) Sum {
	return NewRolling(data).Sum()
}

// NewRolling computes asum and bsum of initial data and returns a new Rolling
// instance.
func NewRolling(data []byte) Rolling {
	asum := Sum(0)
	for _, val := range data {
		asum += Sum(val)
	}

	bsum := Sum(0)
	for i, val := range data {
		bsum += Sum(len(data)-(i+1)+1) * Sum(val)
	}

	ret := Rolling{
		data: make([]byte, len(data)),
		asum: asum % mod,
		bsum: bsum % mod,
	}

	copy(ret.data, data)
	return ret
}
