-include .env
testname := all
SOURCES := $(shell find ./src -type f -name '*.go')
PROTOS := $(shell find ./proto -type f -name '*.proto')
EXECUTABLE := ./src/pexipbox 

SSL_CAKEY_KEY := ./crypto/cakey.key
SSL_CACERT_PEM := ./crypto/cacert.pem
SSL_SERVER_KEY := ./crypto/server.key
SSL_SERVER_CSR := ./crypto/server.csr
SSL_SERVER_PEM := ./crypto/server.pem
SSL_CLIENT_KEY := ./crypto/client.key
SSL_CLIENT_CSR := ./crypto/client.csr
SSL_CLIENT_PEM := ./crypto/client.pem
SERVER_FOLDER := /tmp/dropbox/server
CLIENT_FOLDER := /tmp/dropbox/client

all: $(EXECUTABLE)

src/proto/common.pb.go: $(PROTOS)
	protoc -I=./proto --go-grpc_out=src/proto --go_out=src/proto $(PROTOS)

mocks: src/proto/common.pb.go
	cd src/proto/ && mockery --all

$(EXECUTABLE): $(SOURCES)
	cd src && go build

$(CLIENT_FOLDER):
	mkdir -p $@

$(SERVER_FOLDER):
	mkdir -p $@

$(PEXIPBOX_META_ROOT):
	mkdir -p $@

client: $(EXECUTABLE) | $(CLIENT_FOLDER)
	$(EXECUTABLE) -m client -f $(CLIENT_FOLDER)

server: $(EXECUTABLE) | $(SERVER_FOLDER) $(PEXIPBOX_META_ROOT)
	$(EXECUTABLE) -m server -f $(SERVER_FOLDER)

$(SSL_CAKEY_KEY):
	openssl ecparam -name prime256v1 -genkey -noout -out $(SSL_CAKEY_KEY)

$(SSL_CACERT_PEM): $(SSL_CAKEY_KEY)
	openssl req -x509 -new -nodes -key $(SSL_CAKEY_KEY) -subj "/CN=TestCA/C=MY" -days 730 -out $(SSL_CACERT_PEM)

$(SSL_SERVER_KEY):
	openssl ecparam -name prime256v1 -genkey -noout -out $(SSL_SERVER_KEY)

$(SSL_SERVER_CSR): $(SSL_SERVER_KEY)
	@echo "SSL_SERVER_CSR"
	openssl req -new -key $(SSL_SERVER_KEY) -out $(SSL_SERVER_CSR) -config ./crypto/csr.conf
	
$(SSL_SERVER_PEM): $(SSL_SERVER_CSR) $(SSL_CACERT_PEM)
	openssl x509 -req -in $(SSL_SERVER_CSR) -CA $(SSL_CACERT_PEM) -CAkey $(SSL_CAKEY_KEY) -CAcreateserial -out $(SSL_SERVER_PEM) \
		-days 90 -extfile ./crypto/csr.conf -extensions req_ext

$(SSL_CLIENT_KEY):
	openssl ecparam -name prime256v1 -genkey -noout -out $(SSL_CLIENT_KEY)

$(SSL_CLIENT_CSR): $(SSL_CLIENT_KEY)
	@echo "SSL_CLIENT_CSR"
	openssl req -new -key $(SSL_CLIENT_KEY) -out $(SSL_CLIENT_CSR) -config ./crypto/csrclient.conf

$(SSL_CLIENT_PEM): $(SSL_CAKEY_KEY) $(SSL_CACERT_PEM) $(SSL_CLIENT_CSR)
	openssl x509 -req -in $(SSL_CLIENT_CSR) -CA $(SSL_CACERT_PEM) -CAkey $(SSL_CAKEY_KEY) -CAcreateserial -out $(SSL_CLIENT_PEM) \
		-days 90 -extfile ./crypto/csrclient.conf -extensions req_ext

cryptogen: $(SSL_SERVER_PEM) $(SSL_CLIENT_PEM)

clean:
	pkill pexip || echo 'no process launched'
	rm -rf /tmp/dropbox/{client,server}
	rm -rf $(PEXIPBOX_META_ROOT)

unittest:
	cd src && go test ./...

test: $(EXECUTABLE) $(SSL_SERVER_PEM) $(SSL_CLIENT_PEM) $(EXECUTABLE)
	pkill pexip || echo 'no process launched'
	rm -rf /tmp/dropbox/{client,server}
	mkdir -p /tmp/dropbox/{client,server}
	rm -rf $(PEXIPBOX_META_ROOT)
	mkdir $(PEXIPBOX_META_ROOT)
ifeq ($(testname), all)
	CLIENT_CMD='$(EXECUTABLE) -m client -f /tmp/dropbox/client' \
	SERVER_CMD='$(EXECUTABLE) -m server -f /tmp/dropbox/server' \
		pytest -q -rapP
else
	CLIENT_CMD='$(EXECUTABLE) -m client -f /tmp/dropbox/client' \
	SERVER_CMD='$(EXECUTABLE) -m server -f /tmp/dropbox/server' \
		pytest -vv -s . -k $(testname)
endif
