# PEXIPBOX

This is a dropbox-like project that is done for Pexip. There are two applications: client and
server. Communication between the applications is done on top gRPC, authentication is done with
mutual TLS.

## Getting started

There is a [Makefile](./Makefile) that consists of a couple of targets you can start with:

- `cryptogen` uses openssl to create required metrials for communication between client
  and server.
- `unittest` runs unit tests written in Go.
- `test` runs integration tests written in Pytest. To run a certain test run designate option `testname`
- `server` runs a server
- `client` runs a client

To run integration test suite or the applications make sure that you setup environment variables.
Checkout [.env.dist](./.env.dist) for reference.

### Development environment

The project is developed and tested in **GNU/Linux**. It relies on some functions from Linux API, return
codes, signal codes. List of used tools:

- Go 1.14
- Python 3.8.6
- Pytest 6.2.2
- Linux 5.9.14
- GNU make utility

The project involves some libraries listed in [go.mod](./src/go.mod).

## Known issues

- **A client does nothing if an uploaded file is not verified.**
- **If a server restarts it doesn't actualize indexes.**
- The global index of chunks across all files doesn't rollback committing of uploaded file fails.
- A task of a file is not canceled if a new event of the file appears.
- Logging doesn't support levels. It requires an additional library zap/logger or creating different
  loggers for every level manually.
- Inappropriate data structures which are used for indexation. Particularly B-Tree is replaced by a standard
  hash table. Good to have custom hash tables to store md5 checksums as keys.


## Data flow

Some diagrams to help understanding main component interactions

### Happy path of partial uploading file

The process is triggered by a file system event which is delivered with inotify.
It's not shown on the diagram to save some space. It starts from `FsObserve.Run`.

```mermaid
sequenceDiagram
  participant client
  participant server
  participant part_file
  participant GlobalChunkIndex
  participant PathIndex
  client ->> server: requests checksum of target_file
  server ->> client: md5(target_file)
  server ->> client: weak and strong checksums
  client ->> server: initial request to upload
  server ->> PathIndex: locks target_file for reading
  client ->> server: a reference to chunk0 that already exists
  server ->> part_file: writes data of chunk0 of target_file
  client ->> server: md5(chunk1)
  server ->> GlobalChunkIndex: md5(chunk1)
  GlobalChunkIndex ->> server: a reference to the chunk and the file the chunk in
  server ->> PathIndex: locks the file for reading
  PathIndex ->> server: the path of the locked file
  server ->> part_file: writes data from the chunk of the file
  server ->> PathIndex: unlocks the file for reading
  server ->> client: a number of written bytes [1]
  client ->> server: md5(chunk2)
  server ->> GlobalChunkIndex: md5(chunk2)
  GlobalChunkIndex ->> server: not found
  server ->> client: 0 bytes written
  client ->> server: chunk2
  server ->> part_file: writes data of chunk2
  client ->> server: a termination message
  server ->> PathIndex: unlocks target_file for reading
  server ->> PathIndex: locks target_file for writing
  server ->> part_file: renames to target_file
  server ->> PathIndex: unlocks target_file for writing
  server ->> client: the total number of written bytes
  client ->> server: requests a checksum of target_file
  server ->> PathIndex: locks target_file for reading
  server ->> client: md5(target_file)
  server ->> PathIndex: unlocks target_file for reading
```

References:

[1] In fact number of written bytes is 4096 as we don't consider chunks less than 4096 bytes.

### Happy path of committing a partially uploaded file.

- transaction updates .part file, which replaces the file is being uploaded. Consider it as a main process.
- PathIndex stores sequance numbers and locks of paths of files. 
- ChunkIndexRepo gives access to chunks of a designated file.
- GlobalChunkIndex givers access to any chunk of files by its md5.

```mermaid
sequenceDiagram
  participant transaction
  participant PathIndex
  participant ChunkIndexRepo
  participant GlobalChunkIndex
  participant part_file
  ChunkIndexRepo ->> transaction: checksums of chunks of target_file
  part_file ->> transaction: checksums of chunks
  transaction ->> ChunkIndexRepo: checksums of chunks of part_file
  transaction ->> GlobalChunkIndex: add new checksums
  transaction ->> PathIndex: lock target_file [1]
  transaction ->> part_file: renames to target_file
  transaction ->> PathIndex: unlock target_file [1]
  transaction ->> GlobalChunkIndex: drop invalid checksums
```
[1] All operations of PathIndex, ChunkIndexRepo and GlobalChunkIndex are concurrent-safe but locking
a file is a self-contained operation.
  
